﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessPartnerSLController : ControllerBase
    {
        private readonly IBusssinesPartner _bussinesPartner;
        private readonly IMapper _mapper;
        public BusinessPartnerSLController(IBusssinesPartner busssinesPartner,IMapper mapper)
        {
            _bussinesPartner = busssinesPartner;
            _mapper = mapper;
        }

        [HttpGet("Gruas")]
        public async Task<IActionResult> GetGruas()
        {
            var data = await _bussinesPartner.GetExtraccionista();
            return Ok(data.value);
        }

        [HttpGet("Fletes")]
        public async Task<IActionResult> GetFletes()
        {
            var data = await _bussinesPartner.GetFletero();
            return Ok(data.value);
        }

        [HttpPost]
        public async Task<IActionResult> postBusinessPartner([FromBody]BusinessPartnerSL businessPartnerSL)
        {
            var map= _mapper.Map<businessPartnerSLDTO>(businessPartnerSL);
            return Ok( await _bussinesPartner.PostBussinesPartner(map));
        }

    }
}
