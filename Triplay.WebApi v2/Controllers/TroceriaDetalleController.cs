﻿using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Modelos;
using Triplay.Modelos.Enums;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TroceriaDetalleController : ControllerBase
    {
        private readonly ITroceriaDetalleService _detalle;
        private readonly ITroceriaHeaderService _header;
        public TroceriaDetalleController(ITroceriaDetalleService detalle, ITroceriaHeaderService header)
        {
            _detalle = detalle;
            _header = header;
        }

        [HttpPost]
        public IActionResult Post(TroceriaDetalle detalle)
        {
            if (ModelState.IsValid)
            {
                bool largoCorr = _detalle.sum(detalle);
                if (!largoCorr)
                    return BadRequest("El largo del tronco no corresponde con los trozos");
                var header = _header.Get(detalle.TroceriaId);
                if (header == null)
                    return BadRequest();

                if (header.Estatus == EEstatus.CERRADO)
                {
                    return BadRequest("La tarja ya esta Cerrada");
                }

                _detalle.Add(detalle);
                return Created($"/api/TroceriaDetalla/{detalle.Id}", detalle);
            }
            return BadRequest();

        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_detalle.GetAll(x => x.TroceriaId == id));
        }

        [HttpGet("corte/{etiqueta}")]
        public IActionResult Get(string etiqueta)
        {
            var tronco = _detalle.GetFirst(x => x.Etiqueta == etiqueta);
            if(tronco == null)
            {
                return NotFound();
            }
            if(tronco.estatus == EETronco.DISPONIBLE ||tronco.estatus == EETronco.CORTE)
            {
                return Ok(tronco);
            }
            else
            {
                return BadRequest("Ese Tronco ya no esta Disponible para corte");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var detalle = _detalle.Get(id);
            if (detalle == null)
            {
                return NotFound();
            }
            _detalle.Delete(id);
            return NoContent();

        }

        [HttpPut]
        public IActionResult Put(TroceriaDetalle troceriaDetalle)
        {
            if (ModelState.IsValid)
            {
                bool largoCorr = _detalle.sum(troceriaDetalle);
                if (!largoCorr)
                    return BadRequest("El largo del tronco no corresponde con los trozos");
                var data = _detalle.Get(troceriaDetalle.Id);
                if (data == null)
                {
                    return NotFound();
                }
                _detalle.Update(troceriaDetalle);
                //return Created($"api/triceriadetalle/{troceriaDetalle.Id}", troceriaDetalle);
                return Ok(troceriaDetalle);
            }
            return BadRequest("Modelo Invalido");
        }

        [HttpGet("Conteo")]
        public IActionResult ConteoTroncos(int idTarja)
        {
            var count = _detalle.count(x => x.TroceriaHeader.Id == idTarja);
            return Ok(count);
        }
    }
}
