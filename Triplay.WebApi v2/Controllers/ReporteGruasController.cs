﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReporteGruasController : ControllerBase
    {
        private readonly IReporteGruasService _reporteGruasService;

        public ReporteGruasController(IReporteGruasService reporteGruasService)
        {
            _reporteGruasService = reporteGruasService;
        }

        [HttpGet]
        public IActionResult Get(string fechaini, string? fechafin ="")
        {
            var datosSP = _reporteGruasService.GetDatosSP(fechaini, fechafin);
            return Ok(datosSP);
        }
    }
}
