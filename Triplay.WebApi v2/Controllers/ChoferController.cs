﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Services.Interfaces;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChoferController : ControllerBase
    {

        private readonly IChoferService _choferService;
        private readonly IMapper _mapper;
        public ChoferController(IChoferService choferService, IMapper mapper)
        {
            _choferService = choferService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_mapper.Map<IEnumerable<ChoferDTO>>(_choferService.GetAll(include: "Vehiculos")));
        }

        [HttpGet("busqueda")]
        public IActionResult Get(string? nombre, string? placas)
        {
            /*if (placas == null)
            {*/
            var chofer = _mapper.Map<IEnumerable<ChoferDTO>>(_choferService.busqueda(nombre, placas, include: "Vehiculos"));
            if (chofer.LongCount() == 0)
            {
                return NotFound();
            }
            return Ok(chofer);
            /* }
             else
             {
                 var chofer = _mapper.Map<ChoferDTO>(_choferService.GetFirst(filter: x=>x.Vehiculos.Placas==placas, include: "Vehiculos"));
                 if(chofer== null)
                 {
                     return NotFound();
                 }
                 return Ok(chofer);
             }*/
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var chofer = _choferService.Get(id);
            if (chofer == null)
            {
                return NotFound();
            }
            return Ok(chofer);
        }


        [HttpPost]
        public IActionResult Post(Chofer chofer)
        {
            if (ModelState.IsValid)
            {
                _choferService.Add(chofer);
                return Created($"/api/Chofer/{chofer.Id}", chofer);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }
        [HttpPut]
        public IActionResult Put(Chofer chofer)
        {
            if (ModelState.IsValid)
            {
                _choferService.Update(chofer);
                return Created($"/api/Chofer/{chofer.Id}", chofer);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var cho = _choferService.Get(id);
            if (cho == null)
            {
                return NotFound();
            }
            _choferService.Delete(id);
            return NoContent();
        }


    }
}

