﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlmacenSLController : ControllerBase
    {
        private readonly IAlmacenesSL _almacenesSL;

        public AlmacenSLController(IAlmacenesSL almacenesSL)
        {
          _almacenesSL = almacenesSL;
        }

        [HttpGet]
        public async Task<IActionResult> GetAlmacen()
        {
            var almacenes = await _almacenesSL.GetAlmacenes();
            return Ok(almacenes.value);
        }
    }
}
