﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Servicios.Interfaces;
using PdfSharpCore;
using PdfSharpCore.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharpCore.Pdf.Internal;
using System.Text;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReporteTarjaViewController : ControllerBase
    {
        private readonly IReporteTarjaViewService _reporteTarjaViewService;
        private readonly ITroceriaHeaderService _troceriaHeaderService;
        public ReporteTarjaViewController(IReporteTarjaViewService reporteTarjaViewService,ITroceriaHeaderService troceriaHeaderService)
        {

            _reporteTarjaViewService = reporteTarjaViewService;
            _troceriaHeaderService = troceriaHeaderService;
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var reporte = _reporteTarjaViewService.GetAll(x => x.Id == id);
            return Ok(reporte);
        }

        [HttpGet("csv/{id}")]
        public IActionResult GetCsv(int id)
        {
            var reporte = _reporteTarjaViewService.GetAll(x => x.Id == id);
            if (reporte != null)
            {
                string data = "No.,Especie,D1,D2,LAR,DC,C,AV,TV,TC,AB,AM,CD,SB,SM,LN \n";
                foreach (var row in reporte)
                {
                    data += $"{row.NumTronco},{row.Especie},{row.D1},{row.D2},{row.LAR},{row.DES},{row.CAB},{row.AV},{row.TV},{row.TC},{row.AB},{row.AM},{row.CD},{row.SB},{row.SM},{row.LN}\n";
                }
                var databyte = Encoding.UTF8.GetBytes(data);
                byte[]? response = null;
                using (MemoryStream stream = new MemoryStream())
                {
                    stream.Write(databyte);
                    response = stream.ToArray();
                }

                return File(response, "text/csv");
            }
            return BadRequest("no hay registros");
        }

        [HttpGet("pdf/{id}")]
        public IActionResult GetPdf(int id)
        {
            try
            {
                var reporte = _reporteTarjaViewService.GetAll(x => x.Id == id);
                var header = _troceriaHeaderService.GetFirst(x => x.Id == id, include: "Boleto");
                var headerview = reporte.First();
                var docuemnt = new PdfDocument();
                var AvTotal = Math.Round(reporte.Sum(x => x.AVM3), 3, MidpointRounding.AwayFromZero);
                var TvTotal = Math.Round(reporte.Sum(x => x.TVM3), 3, MidpointRounding.AwayFromZero);
                var TcTotal = Math.Round(reporte.Sum(x => x.TCM3), 3, MidpointRounding.AwayFromZero);
                var AbTotal = Math.Round(reporte.Sum(x => x.ABM3), 3, MidpointRounding.AwayFromZero);
                var AmTotal = Math.Round(reporte.Sum(x => x.AMM3), 3, MidpointRounding.AwayFromZero);
                var CdTotal = Math.Round(reporte.Sum(x => x.CDM3), 3, MidpointRounding.AwayFromZero);
                var SbTotal = Math.Round(reporte.Sum(x => x.SBM3), 3, MidpointRounding.AwayFromZero);
                var SmTotal = Math.Round(reporte.Sum(x => x.SMM3), 3, MidpointRounding.AwayFromZero);
                var LnTotal = Math.Round(reporte.Sum(x => x.LNM3), 3, MidpointRounding.AwayFromZero);
                var M3Total = Math.Round(reporte.Sum(x => x.TotalM3), 3, MidpointRounding.AwayFromZero);
                #region html
                string htmlContent = "<div class='container'>";
                htmlContent += "<p>Fecha de Creacion: " + header.Fecha + "</p>";
                htmlContent += "<h1 class='subrayado' style='text-align: center;'>CARGA RECIBIDA DEL:</h1>";
                htmlContent += "<h1 class='subrayado' style='text-align: center;'>" + header.Propietario + "</h1>";
                htmlContent += "<table style='width: 100%; text-align: center;'>\r\n            <tr>";
                htmlContent += "<td colspan='2'>   <p><b>No. de Tarja:</b></p>\r\n<p><b>" + header.Boleto.Tarja + "</b></p></td>";
                htmlContent += "<td><p><b>No. Guia</b></p>\r\n <p><b>" + header.Boleto.GuiaForestal + "</b></p></td>";
                htmlContent += "<td colspan='2'> <p>Placas:</p>\r\n   <p>" + header.Boleto.Placas + "</p></td>";
                htmlContent += " <td><p>Prop:</p>\r\n                <p>" + header.Propietario + "</p></td>\r\n            </tr>";
                htmlContent += "  <tr>\r\n                <td>   <p>Peso:</p>\r\n                    <p>" + header.Boleto.PesoNeto + "</p></td>\r\n                <td> <p>Trozos Enviados:</p>\r\n                    <p>" + header.Boleto.Trozos + "</p></td>\r\n                <td>  <p>Trozos Recibidos:</p>\r\n                    <p>" + header.TrozosRecibidos + "</p></td>\r\n                <td>   <p>Cabezas:</p>\r\n                    <p>" + header.Cabezas + "</p></td>\r\n                <td>  <p>M3 Enviados:</p>\r\n                    <p>" + header.Boleto.metroscubicos + "</p></td>\r\n                <td> <p>M3 Recibidos:</p>\r\n                    <p>" + header.TrozosRecibidos + "</p></td>\r\n            </tr>";
                htmlContent += "  <tr>\r\n                <td colspan='2'>   <p class='subrayado'><b>Factor:</b></p>\r\n                    <p class='subrayado'><b>" + header.Factor + " KG/M3</b></p></td>\r\n                <td>  <p class='subrayado'><b>Dieferencia :</b></p>\r\n                    <p class='subrayado'><b>" + header.DiferenciaTrozos + " Trozos</b></p></td>\r\n                <td> <p class='subrayado'><b>Rel Cabeszas:</b></p>\r\n                    <p class='subrayado'><b>1 a " + Math.Round((double)header.TrozosRecibidos.Value / header.Cabezas.Value, 2, MidpointRounding.AwayFromZero) + "</b></p></td>\r\n                <td colspan='2'>   <p class='subrayado'><b>Dieferencia M3:</b></p>\r\n                    <p class='subrayado'><b>" + header.DiferenciaMetros + " M3</b></p>\r\n                </tr>";
                htmlContent += " </table>\r\n        <table class='table'>\r\n            <thead>\r\n                <tr>\r\n                    <th style='border-right: 1px dotted white;' colspan='6'></th>\r\n                    <th style='border-right: 1px dotted white;' colspan='9'>LARGOS</th>\r\n                    <th colspan='10'>METROS CUBICOS</th>\r\n                </tr>\r\n                <tr>\r\n                    <th>#T</th>\r\n                    <th>D1</th>\r\n                    <th>D2</th>\r\n                    <th>LA</th>\r\n                    <th>DC</th>\r\n                    <th style='border-right: 1px dotted white;'>C</th>\r\n                    <th>AV</th>\r\n                    <th>TV</th>\r\n                    <th>TC</th>\r\n                    <th>AB</th>\r\n                    <th>AM</th>\r\n                    <th>CD</th>\r\n                    <th>SB</th>\r\n                    <th>SM</th>\r\n                    <th style='border-right: 1px dotted white;'>LN</th>\r\n                    <th>AV</th>\r\n                    <th>TV</th>\r\n                    <th>TC</th>\r\n                    <th>AB</th>\r\n                    <th>AM</th>\r\n                    <th>CD</th>\r\n                    <th>SB</th>\r\n                    <th>SM</th>\r\n                    <th>LN</th>\r\n                    <th>Total</th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>";
                foreach (var tronco in reporte)
                {
                    htmlContent += "<tr>";
                    htmlContent += "<td>" + tronco.NumTronco + "</td>";
                    htmlContent += "<td>" + tronco.D1 + "</td>";
                    htmlContent += "<td>" + tronco.D2 + "</td>";
                    htmlContent += "<td>" + tronco.LAR + "</td>";
                    htmlContent += "<td>" + tronco.DES + "</td>";

                    if (tronco.CAB)
                        htmlContent += "<td style='border-right: 1px dotted black;' >" + "S" + "</td>";
                    else
                    {
                        htmlContent += "<td style='border-right: 1px dotted black;' ></td>";
                    }
                    htmlContent += "<td>" + tronco.AV + "</td>";
                    htmlContent += "<td>" + tronco.TV + "</td>";
                    htmlContent += "<td>" + tronco.TC + "</td>";
                    htmlContent += "<td>" + tronco.AB + "</td>";
                    htmlContent += "<td>" + tronco.AM + "</td>";
                    htmlContent += "<td>" + tronco.CD + "</td>";
                    htmlContent += "<td>" + tronco.SB + "</td>";
                    htmlContent += "<td>" + tronco.SM + "</td>";
                    htmlContent += "<td style='border-right: 1px dotted black;' >" + tronco.LN + "</td>";
                    htmlContent += "<td>" + tronco.AVM3 + "</td>";
                    htmlContent += "<td>" + tronco.TVM3 + "</td>";
                    htmlContent += "<td>" + tronco.TCM3 + "</td>";
                    htmlContent += "<td>" + tronco.ABM3 + "</td>";
                    htmlContent += "<td>" + tronco.AMM3 + "</td>";
                    htmlContent += "<td>" + tronco.CDM3 + "</td>";
                    htmlContent += "<td>" + tronco.SBM3 + "</td>";
                    htmlContent += "<td>" + tronco.SMM3 + "</td>";
                    htmlContent += "<td>" + tronco.LNM3 + "</td>";
                    htmlContent += "<td>" + tronco.TotalM3 + "</td>";
                    htmlContent += "</tr>";
                }
                htmlContent += "</tbody>\r\n            <tfoot>\r\n                <tr>\r\n                    <td colspan='14'></td>\r\n                    <td>Sumas</td>";
                htmlContent += "<td>" + AvTotal + "</td>";
                htmlContent += "<td>" + TvTotal + "</td>";
                htmlContent += "<td>" + TcTotal + "</td>";
                htmlContent += "<td>" + AbTotal + "</td>";
                htmlContent += "<td>" + AmTotal + "</td>";
                htmlContent += "<td>" + CdTotal + "</td>";
                htmlContent += "<td>" + SbTotal + "</td>";
                htmlContent += "<td>" + SmTotal + "</td>";
                htmlContent += "<td>" + LnTotal + "</td>";
                htmlContent += "<td>" + Math.Round(M3Total, 3, MidpointRounding.AwayFromZero) + "</td>";
                htmlContent += "   </tr>\r\n            </tfoot>\r\n        </table>\r\n    </div>";
                #endregion
                PdfGenerator.AddPdfPages(docuemnt, htmlContent, PageSize.A4, 0, PdfGenerator.ParseStyleSheet(" .container {\r\n        width: 100%;\r\n  font-size: 12px;    }\r\n\r\n    .table {\r\n        width: 100%;\r\n        text-align: center;\r\n        font-family: sans-serif;\r\n        border-collapse: collapse;\r\n  font-size: 10px;  }\r\n\r\n    .table th {\r\n        background-color: black;\r\n        color: white;\r\n    }\r\n\r\n\r\n\r\n    .subrayado {\r\n        text-decoration: underline;\r\n    } \r\n    p{\r\n        display: inline;\r\n    }"));
                var especies = reporte.Select(x => x.Especie).Distinct();
                
                var total = 0.0;
                var pag2 = @$" <h1 class='subrayado' style='text-align: center;'>CARGA RECIBIDA DEL:</h1>
       <h1 class='subrayado' class='subrayado' style='text-align: center;'>{header.Propietario}</h1>";


                pag2 += @"     <table style=""width: 50%; text-align: center; margin-left:20%;"">
            <tbody>
               <tr>
               <td colspan=""2""></td>
";

                foreach (var especie in especies)
                {
                    pag2 += @$"  
                <td colspan=""2"">{especie}</td>";
                     
                };
                pag2 += @$"  
            </tr>
        <tr>
            <th class=""subrayado"">Triplay</th>
            <th></th>"
;
                foreach (var especie in especies)
                {
                    pag2 += @$"  
            <th>Pies</th>
            <th  style='border-right: 1px dotted black;'>%</th>";
           };
                pag2 += @$"  
        </tr>
        <tr>
            <td></td>
            <td>Tri.Vista</td>
            ";
                foreach (var especie in especies)
                {
                 
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal+ CdTotal;
                    pag2 += @$"  
                    <td>{Math.Round(TvTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (TvTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(TvTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    }
                }
                pag2 += @$"  
        </tr>
        
        <tr>
            <td></td>
            <td>Tri.Centro</td>
    ";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                    pag2 += @$" 
            <td> {Math.Round(TcTotal * (6 / Math.Pow(.3048, 3)))} </ td >";
                    if (TcTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(TcTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$"  
        
        </tr>
    
        <tr>
            <th class=""subrayado"">Aserradero</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td></td>
            <td>Aviejado</td>
";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                    pag2 += @$" 
            <td>{Math.Round(AvTotal * (6 / Math.Pow(.3048, 3)))}</td >";
                    if (AvTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(AvTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };

                };
                pag2 += @$" 
        </tr>
        <tr>
            <td></td>
            <td>Bueno</td>
";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                    pag2 += @$" 
            <td>{Math.Round(AbTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (AbTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(AbTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
        </tr>
        <tr>
            <td></td>
            <td>Malo</td>";

                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;
                    pag2 += @$" 
            <td>{Math.Round(AmTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (AmTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(AmTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
        </tr>
    
      
        <tr>
            <th class=""subrayado"">Cortas Dim</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
  
            <tr>
                <td></td>
                <td style='black;border-bottom: 1px dotted black '>Cortas Dim</td>           
";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    pag2 += @$" 
                <td style='black;border-bottom: 1px dotted black '>{Math.Round(CdTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (CdTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>{Math.Round((Math.Round(CdTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
        </tr>
       
        <tr>
            <td></td>
            <td></td>
         ";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal+ CdTotal;
                    pag2 += @$"
                     <td>{Math.Round(total * (6 / Math.Pow(.3048, 3)))}</td>
                    <td style='border-right: 1px dotted black;'>100</td>";
                }

                pag2 += @$" 
        </tr>
        <tr>
            <th class=""subrayado"">Secundario</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
  
            <tr>
                <td></td>
                <td>Bueno</td>   
";


                foreach (var especie in especies)
                {
                    SbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SBM3);
                    SmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SMM3);
                    LnTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.LNM3);
                    total = SbTotal + SmTotal + LnTotal;
                    pag2 += @$" 
            
                <td>{Math.Round(SbTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (SbTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(SbTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
        </tr>
        <tr>
            <td></td>
        

            <td>Malo</td>
";
                foreach (var especie in especies)
                {
                    SbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SBM3);
                    SmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SMM3);
                    LnTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.LNM3);
                    total = SbTotal + SmTotal + LnTotal;
                    pag2 += @$" 
            <td>{Math.Round(SmTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (SmTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((Math.Round(SmTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
    </tr>
    <tr>
        <td></td>
        <td style='black;border-bottom: 1px dotted black '>Leña</td>     ";
                foreach (var especie in especies)
                {
                    SbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SBM3);
                    SmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SMM3);
                    LnTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.LNM3);
                    total = SbTotal + SmTotal + LnTotal;
                    pag2 += @$" 

        <td  style='black;border-bottom: 1px dotted black '>{Math.Round(LnTotal * (6 / Math.Pow(.3048, 3)))}</td>";
                    if (LnTotal > 0)
                    {

                        pag2 += @$"
                    <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>{Math.Round((Math.Round(LnTotal * (6 / Math.Pow(.3048, 3))) / Math.Round(total * (6 / Math.Pow(.3048, 3)))) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                    }
                    else
                    {
                        pag2 += @$"
                        <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>0</td>";

                    };
                };
                pag2 += @$" 
</tr>
       
        <tr>
            <td></td>
            <td></td>
";

                foreach (var especie in especies)
                {

                    SbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SBM3);
                    SmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SMM3);
                    LnTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.LNM3);
                    total = SbTotal + SmTotal + LnTotal;
                    pag2 += @$" 

            <td> {Math.Round(total * (6 / Math.Pow(.3048, 3)))} </td>
            <td style = 'border-right: 1px dotted black;'> 100 </td>";
                 }
                pag2 += @$" 
        </tr>
    </tbody>
    
        <tfoot>
            <th class=""subrayado"">Total de Pies</th>
          
                 
            <td></td>";
                foreach (var especie in especies)
                {
                    TvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TVM3);
                    TcTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.TCM3);
                    AvTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AVM3);
                    AbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.ABM3);
                    AmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.AMM3);
                    CdTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.CDM3);
                    SbTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SBM3);
                    SmTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.SMM3);
                    LnTotal = reporte.Where(x => x.Especie == especie).Sum(x => x.LNM3);
                    total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                    pag2 += @$" 
            <td >{Math.Round(total * (6 / Math.Pow(.3048, 3)))}</td>
            <td style='border-right: 1px dotted black;'>100</td>";
                }
                pag2 += @$" 
        </tfoot>
                </table>";
               

                PdfGenerator.AddPdfPages(docuemnt, pag2, PageSize.A4, 2, PdfGenerator.ParseStyleSheet(@" .subrayado {ext-decoration: underline;}  .table {
        width: 100%;
        text-align: center;
        font-family: sans-serif;
        border-collapse: collapse;
        font-size: 8px;
    }"));


                byte[]? response = null;
                using (MemoryStream stream = new MemoryStream())
                {
                    docuemnt.Save(stream);
                    response = stream.ToArray();
                }
                string filename = $"{id}.pdf";
                return File(response, "application/pdf", filename);
            } 
            catch(Exception ex)
            {
                return BadRequest("No hay Registros");
            }
        }


    }
}
