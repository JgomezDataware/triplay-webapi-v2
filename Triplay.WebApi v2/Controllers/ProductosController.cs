﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Modelos;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        private readonly IProductoService _productoService;
        public ProductosController(IProductoService productoService)
        {
                _productoService = productoService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_productoService.GetAll());
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var producto = _productoService.Get(id);
            if (producto == null)
            {
                return NotFound();
            }
            return Ok(producto);
        }

        [HttpPost]
        public IActionResult Post(Producto producto)
        {
            if (ModelState.IsValid)
            {
                _productoService.Add(producto);
                return Created($"/api/Productos/{producto.Id}", producto);
            }
            else
            {
                return BadRequest("Model Invalid");
            }
        }
        [HttpPut]
        public IActionResult Put(Producto producto)
        {
            if (ModelState.IsValid)
            {
                _productoService.Update(producto);
                return Created($"/api/Productos/{producto.Id}", producto);
            }
            else
            {
                return BadRequest("Model Invalid");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var producto = _productoService.Get(id);
            if(producto == null)
            {
                return NotFound();
            }
            _productoService.Delete(id);
            return NoContent();
        }

        [HttpGet("busqueda")]
        public IActionResult Get(string nombre)
        {
            var especie = (_productoService.busqueda(nombre));
            if (especie.LongCount() == 0)
            {
                return NotFound();
            }
            return Ok(especie);
        }
    }
}
