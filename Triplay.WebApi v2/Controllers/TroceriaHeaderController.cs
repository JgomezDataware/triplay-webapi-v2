﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.AccesoDatos.Migrations;
using Triplay.Modelos;
using Triplay.Modelos.Enums;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TroceriaHeaderController : ControllerBase
    {
        private readonly ITroceriaHeaderService _troceriaHeaderService;
        private readonly ITroceriaDetalleService _troceriaDetalleService;
        private readonly IMapper _mapper;
        public TroceriaHeaderController(ITroceriaHeaderService troceriaHeaderService, ITroceriaDetalleService troceriaDetalleService, IMapper mapper)
        {
            _troceriaDetalleService = troceriaDetalleService;
            _troceriaHeaderService = troceriaHeaderService;
            _troceriaDetalleService = troceriaDetalleService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get(EEstatus estatus, DateTime? fecha, string? guia)
        {
            var all = estatus != EEstatus.CERRADO ? _troceriaHeaderService.GetAll(
                filter: x => x.Estatus == estatus,
                include: "Boleto") :
                fecha == null ?
             _troceriaHeaderService.GetAll(
                filter: x => x.Estatus == EEstatus.CERRADO,
                include: "Boleto") :
                 guia == null ? _troceriaHeaderService.GetAll(
                filter: x => x.Estatus == EEstatus.CERRADO && x.Fecha.Date == fecha,
                include: "Boleto") :
            _troceriaHeaderService.GetAll(
                filter: x => x.Estatus == EEstatus.CERRADO && x.Fecha.Date == fecha && x.Boleto.GuiaForestal == guia,
                include: "Boleto");

            var map = _mapper.Map<IEnumerable<TroceriaHeaderDTO>>(all);
            return Ok(map);
        }

        [HttpGet("UltimasCinco")]
        public IActionResult GetTC(EEstatus estatus)
        {
            var all = estatus == EEstatus.ABIERTO ? _troceriaHeaderService.GetLast(
                filter: x => x.Estatus == EEstatus.ABIERTO,
                orderBy: x => x.OrderByDescending(t => t.Fecha),
                include: "Boleto") :
                estatus == EEstatus.CERRADO ?
                _troceriaHeaderService.GetLast(
                filter: x => x.Estatus == EEstatus.CERRADO,
                orderBy: x => x.OrderByDescending(t => t.Fecha),
                include: "Boleto") :
                _troceriaHeaderService.GetLast(
                filter: x => x.Estatus != EEstatus.ABIERTO,
                orderBy: x => x.OrderByDescending(t => t.Fecha),
                include: "Boleto");

            var map = _mapper.Map<IEnumerable<TroceriaHeaderDTO>>(all);
            return Ok(map);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var header = _troceriaHeaderService.GetFirst(x => x.Id == id, include: "Boleto");
            if (header == null)
            {
                return NotFound();
            }
            var map = _mapper.Map<TroceriaHeaderDTO>(header);
            return Ok(map);
        }


        [HttpGet("CerrarTarja/{id}")]
        public IActionResult CerrarTarja(int id)
        {
            var detalle = _troceriaHeaderService.Get(id);
            if (detalle == null)
            {
                return BadRequest("La tarja no cuenta con troncos");
            }
            else
            {
                var data = _troceriaHeaderService.GetFirst(x => x.Id == id);
                if (data == null)
                {
                    return NotFound();
                }
                _troceriaHeaderService.CerrarTarja(id);
                return Ok();
            }
        }



        [HttpPost]
        public IActionResult Post(TroceriaHeader header)
        {
            if (ModelState.IsValid)
            {
                _troceriaHeaderService.Add(header);
                return Created($"/api/TroceriaHeader{header.Id}", header);
            }
            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(TroceriaHeader header)
        {
            var data = _troceriaHeaderService.Get(header.Id);
            if (ModelState.IsValid)
            {
                _troceriaHeaderService.Update(header);
                return Ok();
            }
            return BadRequest();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var operador = _troceriaHeaderService.Get(id);
            if (operador == null)
            {
                return NotFound();
            }
            _troceriaHeaderService.Delete(id);
            return NoContent();
        }
    }
}
