﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CortesController : ControllerBase
    {
        private readonly ICortesService _cortesService;
        private readonly ITroceriaDetalleService _troceriaDetalleService;
        private readonly IMapper _mapper;
        public CortesController(ICortesService cortesService, IMapper mapper)
        {
            _cortesService = cortesService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get(string etiqueta)
        {
            if (_cortesService.IsValid(etiqueta))
            {
                var all = _cortesService.GetAll(filter: x => x.Tronco.Etiqueta == etiqueta, include: "Tronco,Especie");
                if (all == null)
                    return NotFound();
                var map = _mapper.Map<IEnumerable<CortesDTO>>(all);
                return Ok(map);
            }

            else
            {
                return BadRequest("Ese Tronco No Esta Disponible para Dicha Operación");
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public IActionResult Post([FromBody] CortesDTO corte)
        {
            if (ModelState.IsValid)
            {
                var map = _mapper.Map<Cortes>(corte);
                if (_cortesService.IsValid(corte.TroncoEtiqueta))
                {
                    _cortesService.Add(map);
                    return Created("", map);
                }
                return BadRequest("Ese Tronco No Esta Disponible para Dicha Operación");
            }
            return BadRequest();
        }

        [HttpGet("Conteo")]
        public IActionResult ConteoTroncos(int idTronco)
        {
            var count = _cortesService.count(x => x.TroncoId == idTronco);
            return Ok(count);
        }


    }
}
