﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculoController : ControllerBase
    {
        private readonly IVehiculoService _vehiculoService;
        private readonly IMapper _mapper;
        public VehiculoController(IVehiculoService vehiculoService, IMapper mapper)
        {
            _vehiculoService = vehiculoService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var map = _mapper.Map<IEnumerable<VehiculoDTO>>(_vehiculoService.GetAll(include: "Empresa"));
            return Ok(map);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var map = _mapper.Map<VehiculoDTO>(_vehiculoService.GetFirst(x => x.Id == id, include: "Empresa"));
            if (map == null)
            {
                return NotFound();
            }
            return Ok(map);
        }


        [HttpPost]
        public IActionResult Post(Vehiculos vehiculo)
        {
            if (ModelState.IsValid)
            {
                _vehiculoService.Add(vehiculo);
                return Created($"/api/Vehiculo/{vehiculo.Id}", vehiculo);
            }

            return BadRequest();
        }


        [HttpPut]
        public IActionResult Put(Vehiculos vehiculo)
        {
            if (ModelState.IsValid)
            {
                _vehiculoService.Update(vehiculo);
                return Created($"/api/Vehiculo/{vehiculo.Id}", vehiculo);
            }

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var vehiculo = _vehiculoService.Get(id);
            if (vehiculo == null)
            {
                return NotFound();
            }
            _vehiculoService.Delete(id);
            return NoContent();
        }

        [HttpGet("busqueda")]
        public IActionResult Get(string placas)
        {
            var map = _mapper.Map<IEnumerable<VehiculoDTO>>(_vehiculoService.busqueda(placas,include: "Empresa"));
            //var vehiculos = (_vehiculoService.busqueda(placas));
            if (map.LongCount() == 0)
            {
                return NotFound();
            }
            return Ok(map);
        }
    }
}
