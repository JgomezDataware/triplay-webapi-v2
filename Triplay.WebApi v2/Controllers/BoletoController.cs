﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Xml.Linq;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;
using static System.Net.WebRequestMethods;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoletoController : ControllerBase
    {
        private readonly IBoletoService _boletoService;
        private readonly IChoferService _choferService;
        private readonly IMapper _mapper;
        private readonly IpCamaraSettigs ipCamaraSettigs;
        public BoletoController(IBoletoService boletoService, IMapper mapper, IOptions<IpCamaraSettigs> options, IChoferService choferService)
        {
            _boletoService = boletoService;
            _choferService = choferService;
            _mapper = mapper;
            ipCamaraSettigs = options.Value;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var all = _boletoService.GetAll(include: "Empresa,Producto,Chofer", orderBy: x => x.OrderByDescending(x => x.FechaPrimeraPesada));
            var map = _mapper.Map<IEnumerable<BoletoDTO>>(all);
            foreach (BoletoDTO element in map)
            {
                element.CapturasPrimeraPesadaSplit = element.CapturasPrimeraPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
                element.CapturasSegundaPesadaSplit = element.CapturasSegundaPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            return Ok(map);
        }

        [HttpGet("troceria")]
        public IActionResult GetTroceria()
        {
            var all = _boletoService.GetAll(filter: x => x.EsTroceria == true);

            return Ok(all);
        }

        [HttpGet("autoFill")]
        public IActionResult GetFill(string placas)
        {
            var boleto = _boletoService.GetOne(filter: x => x.Placas == placas, orderBy: x => x.OrderByDescending(y => y.Id));
            if (boleto == null)
                return NotFound();
            return Ok(boleto);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var boleto = _boletoService.GetFirst(include: "Empresa,Producto,Chofer", filter: x => x.Id == id);
            if (boleto == null)
            {
                return NotFound();
            }
            var map = _mapper.Map<BoletoDTO>(boleto);
            map.CapturasPrimeraPesadaSplit = map.CapturasPrimeraPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            map.CapturasSegundaPesadaSplit = map.CapturasSegundaPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            return Ok(map);
        }

        [HttpGet("filtro")]
        public IActionResult Filtro(DateTime? fecha, string? placas, string? chofer, string? cardCode)
        {

            var boleto = _boletoService.filtro(fecha, placas,chofer, cardCode);
            if (boleto == null)
            {
                return NotFound();
            }
            var map = _mapper.Map<IEnumerable<BoletoDTO>>(boleto);
            foreach (BoletoDTO element in map)
            {
                element.CapturasPrimeraPesadaSplit = element.CapturasPrimeraPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
                element.CapturasSegundaPesadaSplit = element.CapturasSegundaPesada.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            return Ok(map);
        }

        [HttpPost]
        public IActionResult Post(Boleto boleto)
        {

            if (ModelState.IsValid)
            {
                _boletoService.Add(boleto);
                return Created($"/api/Boleto/{boleto.Id}", boleto);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }
        [HttpPut]
        public IActionResult Put(Boleto boleto)
        {
            var boletoNuevo = _boletoService.Get(boleto.Id);
            if (boletoNuevo == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (boleto.SegundaPesada != null)
                {
                    boleto.Estatus = Modelos.Enums.EEstatus.CERRADO;
                }
                _boletoService.Update(boleto);
                return Get(boleto.Id);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }



        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var boleto = _boletoService.Get(id);
            if (boleto != null)
            {
                _boletoService.Delete(id);
                return NoContent();
            }
            return NotFound();
        }

        [HttpPost("/guiaForestal")]
        public async Task<IActionResult> GuiaForestal([FromForm] prueba prueba)
        {
            try
            {
                var boleto = _boletoService.GetFirst(filter: x => x.Id == prueba.Id);
                if (boleto != null)
                {


                    var nombreguiaForestal = $"{boleto.GuiaForestal}.pdf";
                    var nombreTarja = $"{boleto.Tarja}.pdf";
                    var rutaGuiaForestal = Path.Combine(ipCamaraSettigs.FilePath, nombreguiaForestal);
                    var rutatarja = Path.Combine(ipCamaraSettigs.FilePath, nombreTarja);


                    using (var stream = System.IO.File.Create(rutaGuiaForestal))
                    {
                        await prueba.guiaForestalFile.CopyToAsync(stream);
                    }
                    using (var stream = System.IO.File.Create(rutatarja))
                    {
                        await prueba.tarjaFile.CopyToAsync(stream);
                    }
                    boleto.TarjaRuta = $"DTW_Documentos/{nombreTarja}";
                    boleto.GuiaForestalRuta = $"DTW_Documentos/{nombreguiaForestal}";
                    _boletoService.Update(boleto);
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("/guiaForestal/{name}")]
        public IActionResult File(string name)
        {
            var ruta = Path.Combine(ipCamaraSettigs.FilePath, name + ".pdf");
            byte[] pdfarray = System.IO.File.ReadAllBytes(ruta);
            Base64Data data = new Base64Data();
            data.data = Convert.ToBase64String(pdfarray);
            // var file = System.IO.File.ReadAllBytes(ruta);
            return Ok(data);
        }

        [HttpGet("/imagen")]
        public IActionResult imagen(string imagen)
        {
            var ruta = Path.Combine(ipCamaraSettigs.SnapshotPath, imagen);
            if (System.IO.File.Exists(ruta))
            {
                byte[] img = System.IO.File.ReadAllBytes(ruta);
                string data = Convert.ToBase64String(img);
                Base64Data data64 = new Base64Data();
                data64.data = String.Format("data:image/jpeg;base64,{0}", data);
                return Ok(data64);
            }
            else
            {
                return NotFound();
            }
        }

    }


    public class prueba
    {
        public int Id { get; set; }
        public IFormFile guiaForestalFile { get; set; }
        public IFormFile tarjaFile { get; set; }
    }

    public class Base64Data
    {
        public string data { get; set; }
    }
}

