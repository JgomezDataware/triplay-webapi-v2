﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClasificacionController : ControllerBase
    {
        private readonly IClasificacionService _clasificacionService;

        public ClasificacionController(IClasificacionService clasificacionService)
        {
            _clasificacionService = clasificacionService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_clasificacionService.GetAll());
        }
    }
}
