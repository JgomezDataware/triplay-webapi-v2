﻿using Microsoft.AspNetCore.Mvc;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IPCamaraController : ControllerBase
    {
        private readonly IIPCamaraService _IPCamaraService;

        public IPCamaraController(IIPCamaraService IPCamaraService)
        {
            _IPCamaraService = IPCamaraService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _IPCamaraService.ObtenerCapturas();
                return Ok(result);
            }
            catch(Exception e)
            {
                return Problem(detail: e.Message, statusCode: 500, title: e.Source);
            }
        }

    }
}
