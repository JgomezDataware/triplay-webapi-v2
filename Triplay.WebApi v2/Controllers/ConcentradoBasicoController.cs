﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using PdfSharpCore;
using PdfSharpCore.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using Triplay.DataAccess;
using Triplay.Modelos;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConcentradoBasicoController : ControllerBase
    {
        private readonly TriplayContext _context;
        public ConcentradoBasicoController(TriplayContext context)
        {
            _context = context;
        }

        [HttpGet("{date}")]
        public IActionResult Get(DateTime date)
        {
            var param = new SqlParameter("@date", date);
            return Ok(_context.ConcentradoBasicoDetalle.FromSqlRaw($"SP_CONCENTRADO_BASICO @date",param).ToList());
        }


        [HttpPost]
        public IActionResult GetReporte([FromBody] DateTime date )
        {

            var docuemnt = new PdfDocument();
            var param = new SqlParameter("@date", date);
            var header = _context.ConcentradoBasicoHeader.FromSqlRaw($"SP_CONCENTRADO_BASICO_HEADER @date", param).ToList();
            var detalle = _context.ConcentradoBasicoDetalle.FromSqlRaw($"SP_CONCENTRADO_BASICO @date", param).ToList();
            var AvTotal = detalle.Sum(x => x.TAVM3);
            var TvTotal = detalle.Sum(x => x.TTVM3);
            var TcTotal = detalle.Sum(x => x.TTCM3);
            var AbTotal = detalle.Sum(x => x.TABM3);
            var AmTotal = detalle.Sum(x => x.TAMM3);
            var CdTotal = detalle.Sum(x => x.TCDM3);
            var SbTotal = detalle.Sum(x => x.TSBM3);
            var SmTotal = detalle.Sum(x => x.TSMM3);
            var LnTotal = detalle.Sum(x => x.TLNM3);
        
            string htmlcontet = $@" <h1 class=""center"" >Productora de Triplay S.A. de C.V.</h1>
    <p class=""center""><b>R.F.C PTR-840903-MN9</b></p>
    <h2 class=""center"">CONCENTRADO DE TROCERIA RECIBA DEL DIA:</h2>
    <h3 class=""center"">{date}</h3>
    <table class=""w-100"">
       <tr>
        <th colspan=""5""></th>
        <th style=""border-left: 1px dotted white;border-right: 1px dotted white;"" colspan=""3"">M3</th>
        <th style=""border-left: 1px dotted white;border-right: 1px dotted white;"" colspan=""3"">Pies</th>
        <th></th>
       </tr>
        <tr>
        <th>Placas</th>
        <th>Predio</th>
        <th>Num. Guia</th>
        <th>Num.Tarja</th>
        <th>Num Trozos</th>
        <th>Enviado</th>
        <th>Recibido</th>
        <th>Diferencia</th>
        <th>Enviado</th>
        <th>Recibido</th>
        <th>Diferencia</th>
        <th>Toneladas</th>
    </tr>";
            foreach (var element in header)
            {
                htmlcontet +=
                       @$"
    <tr>
        <td>{element.Placas}</td>
        <td>{element.Origen}</td>
        <td>{element.GuiaForestal}</td>
        <td>{element.Tarja}</td>
        <td>{element.TrozosRecibidos}</td>
        <td>{element.MetrosCubicos}</td>
        <td>{element.MetrosCubicosRecibidos}</td>
        <td>{element.DiferenciaMetros}</td>
        <td>{element.PiesEnviados}</td>
        <td>{element.PiesRecibidos}</td>
        <td>{element.DiferenciaPies}</td>
        <td>
                {element.PesoNeto}</td>
    </tr>
                ";
            }
            htmlcontet += "</table> <br><br><br>";
            var especies = detalle.Select(x => x.Especie).Distinct();
            var total = 0.0;
            var pag2 = @"     <table style=""width: 50%; text-align: center; margin-left:20%;"">
            <tbody>
               <tr>
               <td colspan=""2""></td>
"
            ;

            foreach (var especie in especies)
            {
                pag2 += @$"  
                <td colspan=""2"">{especie}</td>";

            };
            pag2 += @$"  
            </tr>
        <tr>
            <th class=""subrayado"">Triplay</th>
            <th></th>
            "
;
            foreach (var especie in especies)
            {
                pag2 += @$"  
            <th>Pies</th>
            <th  style='border-right: 1px dotted black;'>%</th>";
            };
            pag2 += @$"  
        </tr>
        <tr>
            <td></td>
            <td>Tri.Vista</td>
            ";
            foreach (var especie in especies)
            {

                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;
                pag2 += @$"  
                    <td>{TvTotal}</td>";
                if (TvTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((TvTotal / total)  * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                }
            }
            pag2 += @$"  
        </tr>
        
        <tr>
            <td></td>
            <td>Tri.Centro</td>
    ";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                pag2 += @$" 
            <td> {TcTotal} </ td >";
                if (TcTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((TcTotal  /total ) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$"  
        
        </tr>
    
        <tr>
            <th class=""subrayado"">Aserradero</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td></td>
            <td>Aviejado</td>
";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                pag2 += @$" 
            <td>{AvTotal}</td >";
                if (AvTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((AvTotal  / total) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };

            };
            pag2 += @$" 
        </tr>
        <tr>
            <td></td>
            <td>Bueno</td>
";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                pag2 += @$" 
            <td>{AbTotal}</td>";
                if (AbTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((AbTotal / total) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
        </tr>
        <tr>
            <td></td>
            <td>Malo</td>";

            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;
                pag2 += @$" 
            <td>{AmTotal}</td>";
                if (AmTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((AmTotal  / total)  * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
        </tr>
    
      
        <tr>
            <th class=""subrayado"">Cortas Dim</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
  
            <tr>
                <td></td>
                <td style='black;border-bottom: 1px dotted black '>Cortas Dim</td>           
";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                pag2 += @$" 
                <td style='black;border-bottom: 1px dotted black '>{CdTotal}</td>";
                if (CdTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>{Math.Round((CdTotal / total ) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
        </tr>
       
        <tr>
            <td></td>
            <td></td>
         ";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;
                pag2 += @$"
                     <td>{total}</td>
                    <td style='border-right: 1px dotted black;'>100</td>";
            }

            pag2 += @$" 
        </tr>
        <tr>
            <th class=""subrayado"">Secundario</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
  
            <tr>
                <td></td>
                <td>Bueno</td>   
";


            foreach (var especie in especies)
            {
                SbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSBM3).FirstOrDefault();
                SmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSMM3).FirstOrDefault();
                LnTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TLNM3).FirstOrDefault();
                total = SbTotal + SmTotal + LnTotal;
                pag2 += @$" 
            
                <td>{SbTotal}</td>";
                if (SbTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((SbTotal  / total) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
        </tr>
        <tr>
            <td></td>
        

            <td>Malo</td>
";
            foreach (var especie in especies)
            {
                SbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSBM3).FirstOrDefault();
                SmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSMM3).FirstOrDefault();
                LnTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TLNM3).FirstOrDefault();
                total = SbTotal + SmTotal + LnTotal;
                pag2 += @$" 
            <td>{SmTotal}</td>";
                if (SmTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;'>{Math.Round((SmTotal  / total) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
    </tr>
    <tr>
        <td></td>
        <td style='black;border-bottom: 1px dotted black '>Leña</td>     ";
            foreach (var especie in especies)
            {
                SbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSBM3).FirstOrDefault();
                SmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSMM3).FirstOrDefault();
                LnTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TLNM3).FirstOrDefault();
                total = SbTotal + SmTotal + LnTotal;
                pag2 += @$" 

        <td  style='black;border-bottom: 1px dotted black '>{LnTotal}</td>";
                if (LnTotal > 0)
                {

                    pag2 += @$"
                    <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>{Math.Round((LnTotal   / total) * 100, 2, MidpointRounding.AwayFromZero)}</td>";
                }
                else
                {
                    pag2 += @$"
                        <td style='border-right: 1px dotted black;border-bottom: 1px dotted black;'>0</td>";

                };
            };
            pag2 += @$" 
</tr>
       
        <tr>
            <td></td>
            <td></td>
";

            foreach (var especie in especies)
            {

                SbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSBM3).FirstOrDefault();
                SmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSMM3).FirstOrDefault();
                LnTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TLNM3).FirstOrDefault();
                total = SbTotal + SmTotal + LnTotal;
                pag2 += @$" 

            <td> {total} </td>
            <td style = 'border-right: 1px dotted black;'> 100 </td>";
            }
            pag2 += @$" 
        </tr>
    </tbody>
    
        <tfoot>
            <th class=""subrayado"">Total de Pies</th>
          
                 
            <td></td>";
            foreach (var especie in especies)
            {
                TvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTVM3).FirstOrDefault();
                TcTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TTCM3).FirstOrDefault();
                AvTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAVM3).FirstOrDefault();
                AbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TABM3).FirstOrDefault();
                AmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TAMM3).FirstOrDefault();
                CdTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TCDM3).FirstOrDefault();
                SbTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSBM3).FirstOrDefault();
                SmTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TSMM3).FirstOrDefault();
                LnTotal = detalle.Where(x => x.Especie == especie).Select(x => x.TLNM3).FirstOrDefault();
                total = TvTotal + TcTotal + AvTotal + AmTotal + AbTotal + CdTotal;

                pag2 += @$" 
            <td >{total}</td>
            <td style='border-right: 1px dotted black;'>100</td>";
            }
            pag2 += @$" 
        </tfoot>
                </table>";

            htmlcontet +=  pag2;

            PdfGenerator.AddPdfPages(docuemnt, htmlcontet, PageSize.A4, 0, PdfGenerator.ParseStyleSheet(@" .center{
        text-align: center;
    }
    h1,h2,h3,h4{
        text-transform: uppercase;
    }
    .w-100{
        width: 100% !important;
    }
     th {
        background-color: black;
        color: white;
    }
     h1{
        font-family: sans-serif;
        font-size: 14px;
    }
  h2{
        font-family: sans-serif;
        font-size: 12px;
    }
  h3{
        font-family: sans-serif;
        font-size: 11px;
    }
    table {
        width: 100%;
        text-align: center;
        font-family: sans-serif;
        border-collapse: collapse;
        font-size: 10px;
    }
"));
            byte[]? response = null;
            using (MemoryStream stream = new MemoryStream())
            {
                docuemnt.Save(stream);
                response = stream.ToArray();
            }
            string filename = $"{date}.pdf";
            return File(response, "application/pdf", filename);
        }


    }
}
