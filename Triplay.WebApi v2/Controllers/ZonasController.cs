﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZonasController : ControllerBase
    {
        private readonly IZonasService _zonaService;

        public ZonasController(IZonasService zonaService)
        {
            _zonaService = zonaService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_zonaService.GetAll());
        }

       
        [HttpPost]
        public IActionResult Post([FromBody] Zonas zona)
        {
            if (ModelState.IsValid)
            {
                _zonaService.Add(zona);
                return Created($"/api/Zonas/{zona.Id}", zona);
            }
            return BadRequest("La zona no se pudo crear");
        }



        [HttpPut]
        public IActionResult Put([FromBody] Zonas zona)
        {
            var  zo =  _zonaService.Get(zona.Id);
            if (zo == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _zonaService.Update(zona);
                return Created($"/api/Zonas/{zona.Id}", zona);
            }
            return BadRequest("La zona no se pudo crear");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var zona = _zonaService.GetFirst(x=>x.Id == id);
            if(zona == null)
            {
                return NotFound();
            }
            _zonaService.Delete(zona);
            return NoContent();
        }

        [HttpGet ("{id}")]
        public IActionResult Get(int id)
        {
            var zona = _zonaService.GetFirst(x => x.Id == id);
            if (zona == null) {
                return NotFound();
            }
            
            return Ok(zona);
        }


    }
}
