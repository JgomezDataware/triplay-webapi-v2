﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EspecieController : ControllerBase
    {
        private readonly IEspecieService _especieService;
        public EspecieController(IEspecieService especieService)
        {
            _especieService = especieService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_especieService.GetAll());
        }

        [HttpGet("busqueda")]
        public IActionResult Get(string codigo)
        {
            var especie =(_especieService.busqueda(codigo));
            if (especie.LongCount() == 0)
            {
                return NotFound();
            }
            return Ok(especie);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var especie = _especieService.Get(id);
            if (especie == null)
            {
                return NotFound();
            }
            return Ok(especie);
        }


            [HttpPost]
        public IActionResult Post(Especie especie)
        {
            if (ModelState.IsValid)
            {
                _especieService.Add(especie);
                return Created($"/api/especie/{especie.Id}", especie);
            }
            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(Especie especie)
        {
            if (ModelState.IsValid)
            {
                _especieService.Update(especie);
                return Created($"/api/especie/{especie.Id}", especie);
            }
            return BadRequest();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var especie = _especieService.Get(id);
            if(especie == null)
            {
                return NotFound();
            }
            _especieService.Delete(id);
            return NoContent();
        }

    }
}
