﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperadorController : ControllerBase
    {
        private readonly IOperadorService _operadorService;
        private readonly IMapper _mapper;
        public OperadorController(IOperadorService operadorService,IMapper mapper)
        {
            _operadorService = operadorService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var map = _mapper.Map<IEnumerable<OperadorDTO>>(_operadorService.GetAll());
            return Ok(map);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var map = _mapper.Map<OperadorDTO>(_operadorService.Get(id));
            if(map == null)
            {
                return NotFound();
            }
            return Ok(map);
        }


        [HttpPost]
        public IActionResult Post(Operadores operador)
        {
            if (ModelState.IsValid)
            {
                _operadorService.Add(operador);
                return Created($"/api/Operador/{operador.Id}", operador);

            }
            else
            {
                return BadRequest("Model Invalid");
            }
        }

        [HttpPut]
        public IActionResult Put(Operadores operador)
        {
            if (ModelState.IsValid)
            {
                _operadorService.Update(operador);
                return Created($"/api/Operador/{operador.Id}", operador);

            }
            else
            {
                return BadRequest("Model Invalid");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var operador = _operadorService.Get(id);
            if(operador == null)
            {
                return NotFound();
            }
            _operadorService.Delete(id);
            return NoContent();
        }
    }
}
