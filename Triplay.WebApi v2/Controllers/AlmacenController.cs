﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlmacenController : ControllerBase
    {
        private readonly IAlmacenService _almacenService;
        
        public AlmacenController(IAlmacenService almacenService)
        {
            _almacenService = almacenService;
        
        }

        [HttpGet]
        public IActionResult Get()
        {
            //select * from
            return Ok(_almacenService.GetAll());
         
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var almacen = _almacenService.Get(id);
            if (almacen == null)
            {
                return NotFound();
            }
            return Ok(almacen);
        }

        [HttpPost]
        public IActionResult Post(Almacen almacen)
        {
            if (ModelState.IsValid)
            {
                _almacenService.Add(almacen);
                return Created($"/api/Almacen/{almacen.Id}", almacen);
            }
            return BadRequest();
        }

        [HttpPut]
        public IActionResult Put(Almacen almacen)
        {
            if (ModelState.IsValid)
            {
                _almacenService.Update(almacen);
                return Created($"/api/Almacen/{almacen.Id}", almacen);
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var almacen = _almacenService.Get(id);
            if (almacen == null)
            {
                return NotFound();
            }
            _almacenService.Delete(id);
            return NoContent();
        }
    }
}
