﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Services.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ImpresoraController : ControllerBase
    {
        private readonly IImpresoraService _impresoraService;
        public readonly IMapper _mapper;

        public ImpresoraController(IImpresoraService impresoraService, IMapper mapper)
        {
            _impresoraService = impresoraService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_mapper.Map<IEnumerable<ImpresoraDTO>>(_impresoraService.GetAll()));
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var impresora = _mapper.Map<ImpresoraDTO>(_impresoraService.Get(id));
            if (impresora == null)
            {
                return NotFound();
            }
            return Ok(impresora);
        }
        [HttpPost]
        public IActionResult Post(Impresora impresora)
        {
            if (ModelState.IsValid)
            {
                _impresoraService.Add(impresora);
                return Created($"/api/Impresora/{impresora.Id}", impresora);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }

        [HttpPut]
        public IActionResult Put(Impresora impresora)
        {
            if (ModelState.IsValid)
            {
                _impresoraService.Update(impresora);
                return Created($"/api/Empresa/{impresora.Id}", impresora);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var impresora = _impresoraService.Get(id);
            if (impresora == null)
            {
                return NotFound();
            }
            _impresoraService.Delete(id);
            return NoContent();
        }

    }
}
