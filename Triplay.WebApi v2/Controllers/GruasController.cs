﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GruasController : ControllerBase
    {

        private readonly IGruasService _gruasService;
        public GruasController(IGruasService gruasService)
        {
            _gruasService = gruasService;
        }

        [HttpGet]
        public IActionResult GetGruas()
        {
            return Ok(_gruasService.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetGruas(int id)
        {
            return Ok(_gruasService.GetFirst(x=>x.Id == id));
        }
        [HttpPost]
        public IActionResult PostGruas([FromBody] Gruas grua)
        {

            if (ModelState.IsValid)
            {
                _gruasService.Add(grua);
                return Created($"/api/Gruas/{grua.Id}",grua);
            }
            return BadRequest();
        }

        [HttpPut]
        public IActionResult PutGruas([FromBody] Gruas gruas)
        {
            if (ModelState.IsValid)
            {
                _gruasService.Update(gruas);
                return Created($"/api/Gruas/{gruas.Id}", gruas);
            }
            return BadRequest();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteGruas(int id)
        {
            var grua = _gruasService.Get(id);
            if (grua != null)
            {
                _gruasService.Delete(id);
                return NoContent();
            }
            return NotFound();
        }
    }
}
