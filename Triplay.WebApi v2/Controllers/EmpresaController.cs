﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Services.Interfaces;

namespace Triplay.WebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        private readonly IEmpresaService _empresaService;
        private readonly IMapper _mapper;
        public EmpresaController(IEmpresaService empresaService,IMapper mapper)
        {
            _empresaService = empresaService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_mapper.Map<IEnumerable<EmpresaDTO>>(_empresaService.GetAll()));
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var empresa = _mapper.Map<EmpresaDTO>(_empresaService.Get(id));
            if(empresa == null)
            {
                return NotFound();

               
            }
            return Ok(empresa);
        }
        [HttpPost]
        public IActionResult Post(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                _empresaService.Add(empresa);
                return Created($"/api/Empresa/{empresa.Id}", empresa);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }

        [HttpPut]
        public IActionResult Put(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                _empresaService.Update(empresa);
                return Created($"/api/Empresa/{empresa.Id}", empresa);
            }
            else
            {
                return BadRequest("Invalid Model");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var empresa = _empresaService.Get(id);
            if (empresa == null)
            {
                return NotFound();
            }
            _empresaService.Delete(id);
            return NoContent();
        }
    }
}
