using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Triplay.AccesoDatos.DTOs;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Respositorio.Implementacion;
using Triplay.Respositorio.Interfaces;
using Triplay.Services.Implementation;
using Triplay.Services.Interfaces;
using Triplay.Servicios.Implementation;
using Triplay.Servicios.Interfaces;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors();
builder.Services.AddDbContext<TriplayContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("Triplay"));
});
builder.Services.Configure<IpCamaraSettigs>(builder.Configuration.GetSection("IpCamaraSettigs"));
builder.Services.AddAutoMapper(typeof(MappingProfile));
builder.Services.AddScoped<IUnidadDeTrabajo, UnidadDeTrabajo>();
builder.Services.AddScoped<IEmpresaService,EmpresaService>();
builder.Services.AddScoped<IProductoService, ProductoService>();
builder.Services.AddScoped<IOperadorService, OperadorService>();
builder.Services.AddScoped<IVehiculoService,VehiculoService>();
builder.Services.AddScoped<IChoferService, ChoferService>();
builder.Services.AddScoped<IBoletoService, BoletoService>();
builder.Services.AddScoped<IIPCamaraService, IPCamaraService>();
builder.Services.AddScoped<IAlmacenService, AlmacenService>();
builder.Services.AddScoped<IEspecieService, EspecieService>();
builder.Services.AddScoped<IZonasService, ZonasService>();
builder.Services.AddScoped<ITroceriaDetalleService, TroceriaDetalleService>();
builder.Services.AddScoped<ITroceriaHeaderService, TroceriaHeaderService>();
builder.Services.AddScoped<IImpresoraService, ImpresoraService>();
builder.Services.AddScoped<IReporteTarjaViewService, ReporteTarjaviewServivce>();
builder.Services.AddScoped<IReporteGruasService, ReporteGruasService>();
builder.Services.AddScoped<IGruasService, GruasService>();
builder.Services.AddScoped<ICortesService, CortesService>();
builder.Services.AddScoped<IClasificacionService, ClasificacionService>();
builder.Services.AddScoped<IServiceLayerAuth, ServiceLayerAuth>();
builder.Services.AddScoped<IAlmacenesSL, AlmacenesSL>();
builder.Services.AddScoped<IBusssinesPartner, BussinesPartner>();

builder.Services.AddCors();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

var snapshotPath = builder.Configuration.GetSection("IpCamaraSettigs").GetSection("snapshotPath").Value;
var filePath =  builder.Configuration.GetSection("IpCamaraSettigs").GetSection("filePath").Value;
if (!Directory.Exists(snapshotPath))
{
    Directory.CreateDirectory(snapshotPath);
}
if (!Directory.Exists(filePath))
{
    Directory.CreateDirectory(filePath);
}
var staticFilesOptions = new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(snapshotPath),
    RequestPath = "/DTW_CapturasCamarasIP"
};

var staticPDFOptions = new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(filePath),
    RequestPath = "/DTW_Documentos"
};



app.UseStaticFiles(staticFilesOptions);
app.UseStaticFiles(staticPDFOptions);
app.UseHttpsRedirection();
app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
app.UseAuthorization();

app.MapControllers();

app.Run();
