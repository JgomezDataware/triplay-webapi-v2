﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.AccesoDatos.DTOs
{
    public class ChoferDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string VehiculosPlacas { get; set; }
        public int VehiculoId { get; set; }
        public int EmpresaId { get; set; }
    }
}
