﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.AccesoDatos.DTOs
{
#nullable enable
    public class OperadorDTO

    { 
    public int Id { get; set; }
    public string Nombre { get; set; }
    
    public string Acceso { get; set; }
   
    public int Turno { get; set; }
   
    public int   Tipo_Usuario { get; set; }
}
}
