﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.AccesoDatos.DTOs
{
    public class CortesDTO
    {
        public double D1 { get; set; }
    
        public double D2 { get; set; }
 
        public double LAR { get; set; }
      
        public int EspecieId { get; set; }

        public string? EspecieNombre { get; set; }

        public int TroncoId { get; set; }

        public string? TroncoEtiqueta { get; set; }

        public EClasificacion Clasificacion { get; set; }
        public int? Descuento { get; set; }
        public string Etiqueta { get; set; }
        public EColor? Color { get; set; } 
    }
}
