﻿namespace Triplay.AccesoDatos.DTOs
{
    sealed public class CapturaCamaraIPDTO
    {
        public string NombreImagen { get; set; } = string.Empty;
        public string UrlImagen { get; set; } = string.Empty;
    }
}
