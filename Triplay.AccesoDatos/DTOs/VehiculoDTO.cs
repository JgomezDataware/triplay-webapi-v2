﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.AccesoDatos.DTOs
{
    public class VehiculoDTO
    {
        public int Id { get; set; }
        public string Placas { get; set; }
       
        public string Chofer { get; set; }

        public int OperadorId { get; set; }
        public string OperadorNombre { get; set; }

        public int EmpresaId { get; set; }
       
        public string EmpresaNombre { get; set; }

        public DateTime Fecha { get; set; } = DateTime.Now;
    }
}
