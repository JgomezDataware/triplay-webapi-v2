﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.AccesoDatos.DTOs
{
    public class BoletoDTO
    {
        public int Id { get; set; }
        public int EmpresaId { get; set; }

        public string EmpresaNombre { get; set; }
        [Required]
        public int ProductoId { get; set; }

        public string ProductoNombre { get; set; }
        [Required]
        public int PrimerOperador { get; set; }

        public string Operador1Nombre { get; set; }
        public int SegundoOperador { get; set; }

        public string Operador2Nombre { get; set; }
        [Required]
        public int ChoferId { get; set; }

        public string ChoferNombre { get; set; }
        public string Placas { get; set; }
        public double PrimeraPesada { get; set; }
        public double SegundaPesada { get; set; }
        public DateTime FechaPrimeraPesada { get; set; }
        public DateTime FechaSegundaPesada { get; set; }
        public double PesoNeto { get; set; }
        public string PrimerTipo { get; set; }
        public string SegundoTipo { get; set; }

        public string CapturasPrimeraPesada { get; set; }
        public List<string> CapturasPrimeraPesadaSplit { get; set; }
        public string CapturasSegundaPesada { get; set; }
        public List<string> CapturasSegundaPesadaSplit { get; set; }

        public string? GuiaForestal { get; set; }
        public string? GuiaForestalRuta { get; set; }
        public string? Tarja { get; set; }
        public string? TarjaRuta { get; set; }
        public string? Almacen { get; set; }
        public string? FolioConsecutivo {get;set;}
        public double? metroscubicos { get; set; }

        public bool esTroceria { get; set; }
        public int? Trozos { get; set; }
        public int Estatus { get; set; }
        
    }
}
