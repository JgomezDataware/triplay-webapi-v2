﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.AccesoDatos.DTOs
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateMap<Empresa, EmpresaDTO>().ReverseMap();
            CreateMap<Operadores, OperadorDTO>().ReverseMap();
            CreateMap<Vehiculos, VehiculoDTO>().ReverseMap();
            CreateMap<Boleto, BoletoDTO>().ReverseMap();
            CreateMap<Chofer, ChoferDTO>().ForMember(p => p.EmpresaId, x => x.MapFrom(y => y.Vehiculos.EmpresaId));
            CreateMap<Impresora, ImpresoraDTO>().ReverseMap();
            CreateMap<TroceriaHeader, TroceriaHeaderDTO>().
                ForMember(p => p.GuiaForestal, x => x.MapFrom(c => c.Boleto.GuiaForestal))
               .ForMember(p => p.Tarja, x => x.MapFrom(c => c.Boleto.Tarja))
               .ForMember(p => p.MetrosCubicos, x => x.MapFrom(c => c.Boleto.metroscubicos))
               .ForMember(p => p.Trozos, x => x.MapFrom(c => c.Boleto.Trozos))
               .ForMember(p => p.Placas, x => x.MapFrom(c => c.Boleto.Placas))
               .ForMember(p => p.Peso, x => x.MapFrom(c => c.Boleto.PesoNeto));

            CreateMap<Cortes, CortesDTO>().ReverseMap();
            //.ForMember(p => p.Color, x => x.MapFrom(c => c.Color.ToString()));
            CreateMap<BusinessPartnerSL, businessPartnerSLDTO>().ReverseMap();
            CreateMap<CortesDTO, Cortes>();


        }
    }
}
