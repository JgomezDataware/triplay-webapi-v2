﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.AccesoDatos.DTOs
{
    public class businessPartnerSLDTO
    {
        public string CardName { get; set; } = string.Empty;
        public string CardType { get; } = "S";
        public string FederalTaxID { get; set; } = "XAXX010101000";
        public string Frozen { get; } = "Y";
        public int Series { get; } = 80;
        public string Properties1 { get; set; } 
        public string Properties2 { get; set; } 

    }
}
