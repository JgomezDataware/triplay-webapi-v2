﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.AccesoDatos.DTOs
{
    public class ImpresoraDTO
    {
        public string Id { get; set; }
        public string DireccionIp { get; set; }
        public string DireccionPort { get; set; }
        public EColor Descripcion { get; set; }
        public bool Troceria { get; set; }
    }
}
