﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.AccesoDatos.DTOs
{
    public class Grua
    {
        public string? Codigo { get; set; }
        public double? Pies { get; set; }
        public string GuiaForestal { get; set; }
        public string TipoTrabajo { get; set; }
    }
}
