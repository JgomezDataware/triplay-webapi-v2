﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.AccesoDatos.DTOs
{
    public class TroceriaHeaderDTO
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public string? Origen { get; set; }
        public string? Propietario { get; set; }
        public int OperadorId { get; set; }
        public string OperadorNombre { get; set; }
        public int BoletoId { get; set; }
        public string Tarja { get; set; }
        public string GuiaForestal { get; set; }
        public int Trozos { get; set; }
        public double MetrosCubicos { get; set; }
        public string Placas { get; set; }
        public double Peso { get; set; }
        public string? GC { get; set; }
        public string? GAC { get; set; }
        public string? GA { get; set; }
        public string? GAD { get; set; }
        public EEstatus Estatus { get; set; }

    }
}
