﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class SP_Concentrado_Basico_Header_SP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE SP_CONCENTRADO_BASICO_HEADER
	-- Add the parameters for the stored procedure here
	@date date
AS
BEGIN

drop table if exists TempIds
 select Id into TempIds from (
 select Troceria.id,Convert(date,Fecha) Fecha from Troceria
    inner join Boletos on BoletoId = Boletos.Id
	WHERE Troceria.Estatus = 1
   )  as T1
	where Fecha = @date
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  	select Placas,Origen, GuiaForestal,Tarja,Trozos,TrozosRecibidos,
	metroscubicos,MetrosCubicosRecibidos,DiferenciaMetros, 
     ROUND(metroscubicos*(6/POWER(.3048,3)),3) as PiesEnviados,
     ROUND(MetrosCubicosRecibidos*(6/POWER(.3048,3)),3) as PiesRecibidos,
     ROUND(DiferenciaMetros*(6/POWER(.3048,3)),3) as DiferenciaPies
    ,  PesoNeto from Troceria
    inner join Boletos on BoletoId = Boletos.Id
	where Troceria.Id  in (select Id from TempIds)

	END";
            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
