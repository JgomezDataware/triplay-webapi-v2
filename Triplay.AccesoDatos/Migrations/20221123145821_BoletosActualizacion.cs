﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class BoletosActualizacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Segunda_Pesada",
                table: "Boletos",
                newName: "SegundaPesada");

            migrationBuilder.RenameColumn(
                name: "Primera_Pesada",
                table: "Boletos",
                newName: "PrimeraPesada");

            migrationBuilder.RenameColumn(
                name: "OperadorId",
                table: "Boletos",
                newName: "SegundoOperador");

            migrationBuilder.AddColumn<string>(
                name: "Placas",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "PrimerOperador",
                table: "Boletos",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PrimerTipo",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SegundoTipo",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Placas",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "PrimerOperador",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "PrimerTipo",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "SegundoTipo",
                table: "Boletos");

            migrationBuilder.RenameColumn(
                name: "SegundoOperador",
                table: "Boletos",
                newName: "OperadorId");

            migrationBuilder.RenameColumn(
                name: "SegundaPesada",
                table: "Boletos",
                newName: "Segunda_Pesada");

            migrationBuilder.RenameColumn(
                name: "PrimeraPesada",
                table: "Boletos",
                newName: "Primera_Pesada");

        }
    }
}
