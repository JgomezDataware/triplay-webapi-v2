﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class añadecodigoEspecies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Codigo",
                table: "Especies",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Codigo",
                table: "Especies");
        }
    }
}
