﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class ReporteTarja : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var view = @"Create view ReporteTarjaview  AS
select Id,Fecha,Tarja,GuiaForestal,Placas,PesoNeto,Trozos,TrozosRecibidos,Cabezas,MetrosCubicosRecibidos,MetrosCubicosEnviados,DiferenciaTrozos,DiferenciaMetros
,Factor,NumTronco,Especie,D1,D2,LAR,DES,AV,TV,TC,AB,AM,CD,SB,SM,LN,CAB,AVM3,TVM3,TCM3,ABM3,AMM3,CDM3,SBM3,SMM3,LNM3, Round((AVM3+TVM3+TCM3+ABM3+AMM3+CDM3+SBM3+SMM3+LNM3),3) as TotalM3
from
(
select * , 
Round(((3.1416 * LN * 0.3048) / 12 * ((POWER(DiametroMayorSM,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM+LN)/LAR),2)+DiametroMayorSM*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM+LN)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as LNM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM+LN)/LAR)  DiametroMayorLN
From(
select *,
Round(((3.1416 * SM * 0.3048) / 12 * ((POWER(DiametroMayorSB,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM)/LAR),2)+DiametroMayorSB*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as SMM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB+SM)/LAR)  DiametroMayorSM
from(
select  *,
Round(((3.1416 * SB * 0.3048) / 12 * ((POWER(DiametroMayorCD,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB)/LAR),2)+DiametroMayorCD*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as SBM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD+SB)/LAR)  DiametroMayorSB
from
(
select *,
Round(((3.1416 * CD * 0.3048) / 12 * ((POWER(DiametroMayorAM,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD)/LAR),2)+DiametroMayorAM*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as CDM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM+CD)/LAR)  DiametroMayorCD
From(
Select *,
Round(((3.1416 * AM * 0.3048) / 12 * ((POWER(DiametroMayorAB,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM)/LAR),2)+DiametroMayorAB*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as AMM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB+AM)/LAR)  DiametroMayorAM
From(
Select *,
Round(((3.1416 * AB * 0.3048) / 12 * ((POWER(DiametroMayorTC,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB)/LAR),2)+DiametroMayorTC*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as ABM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC+AB)/LAR)  DiametroMayorAB
from(
select *,
Round(((3.1416 * TC * 0.3048) / 12 * ((POWER(DiametroMayorTV,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC)/LAR),2)+DiametroMayorTV*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as TCM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV+TC)/LAR)  DiametroMayorTC
from(
Select *,
Round(((3.1416 * TV * 0.3048) / 12 * ((POWER(DiametroMayorAV,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV)/LAR),2)+DiametroMayorAV*(DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV)/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as TVM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*(AV+TV)/LAR)  DiametroMayorTV
from(
Select Id,Fecha,Tarja,GuiaForestal,Placas,Propietario,PesoNeto,Trozos,TrozosRecibidos,Cabezas,
MetrosCubicosEnviados,MetrosCubicosRecibidos,Factor,DiferenciaTrozos,DiferenciaMetros,
NumTronco as NumTronco,T1.Especie,D1,D2,LAR,DES,AV,TV,TC,AB,AM,CD,SB,SM,LN,CAB,CubicajeCilindro,
CubicajeCono,DiametroMayor,DiametroMenor, 
Round(((3.1416 * AV * 0.3048) / 12 * ((POWER(DiametroMayor,2)+POWER(DiametroMayor-((DiametroMayor-DiametroMenor)*AV/LAR),2)+D1*(DiametroMayor-((DiametroMayor-DiametroMenor)*AV/LAR)))/10000)*(1-DES/100) * (CubicajeCilindro/CubicajeCono)),3) as AVM3,
DiametroMayor-((DiametroMayor-DiametroMenor)*AV/LAR)  DiametroMayorAV
From(
Select Troceria.Id,Fecha,Tarja,GuiaForestal,Placas,Propietario,PesoNeto,Trozos,TrozosRecibidos,Cabezas,
Boletos.metroscubicos as MetrosCubicosEnviados,MetrosCubicosRecibidos,Factor,DiferenciaTrozos,DiferenciaMetros,
TroceriaDetalle.Id as NumTronco,Especies.Nombre as Especie,D1,D2,LAR,DES,AV,TV,TC,AB,AM,CD,SB,SM,LN,CAB,TroceriaDetalle.MetrosCubicos as CubicajeCilindro, 
 Round((3.1416 * LAR * 0.3048) / 12 * ((POWER(D1,2)+POWER(D2,2)+D1*D2)/10000)*(1-DES/100),3) as CubicajeCono,
(Select
CASE
WHEN D1>D2 then D1
WHEN D2>D1 then D2
ELSE D1
END as  DiametroMayor)
as DiametroMayor,
(Select
CASE
WHEN D1<D2 then D1
WHEN D2<D1 then D2
ELSE D1
END as DiametroMenor)
as DiametroMenor
From Troceria
LEFT JOIN Boletos on BoletoId = Boletos.Id
LEft Join TroceriaDetalle on Troceria.Id = TroceriaId
Left Join Especies on TroceriaDetalle.EspecieId = Especies.Id
)as T1

) as T2
) as T3
) as T4
) as T5
) as T6
) as T7
) as T8
) as T9
) as T0
";
            migrationBuilder.Sql(view);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           
        }
    }
}
