﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class troncosRecibidos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Cabezas",
                table: "Troceria",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DiferenciaMetros",
                table: "Troceria",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DiferenciaTrozos",
                table: "Troceria",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Factor",
                table: "Troceria",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "MetrosCubicosRecibidos",
                table: "Troceria",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrozosRecibidos",
                table: "Troceria",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cabezas",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "DiferenciaMetros",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "DiferenciaTrozos",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "Factor",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "MetrosCubicosRecibidos",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "TrozosRecibidos",
                table: "Troceria");
        }
    }
}
