﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class TroceriaRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TroceriaId",
                table: "TroceriaDetalle",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TroceriaDetalle_TroceriaId",
                table: "TroceriaDetalle",
                column: "TroceriaId");

            migrationBuilder.AddForeignKey(
                name: "FK_TroceriaDetalle_Troceria_TroceriaId",
                table: "TroceriaDetalle",
                column: "TroceriaId",
                principalTable: "Troceria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TroceriaDetalle_Troceria_TroceriaId",
                table: "TroceriaDetalle");

            migrationBuilder.DropIndex(
                name: "IX_TroceriaDetalle_TroceriaId",
                table: "TroceriaDetalle");

            migrationBuilder.DropColumn(
                name: "TroceriaId",
                table: "TroceriaDetalle");
        }
    }
}
