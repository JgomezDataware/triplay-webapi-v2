﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class cortes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Etiqueta",
                table: "TroceriaDetalle",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateTable(
                name: "Cortes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    D1 = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: false),
                    D2 = table.Column<double>(type: "float", nullable: false),
                    LAR = table.Column<double>(type: "float", nullable: false),
                    EspecieId = table.Column<int>(type: "int", nullable: false),
                    DES = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: false),
                    AV = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    TV = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    TC = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    AB = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    AM = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    CD = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    SB = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    SM = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    LN = table.Column<double>(type: "float(2)", precision: 2, scale: 2, nullable: true),
                    Etiqueta = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cortes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cortes_Especies_EspecieId",
                        column: x => x.EspecieId,
                        principalTable: "Especies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

          

            migrationBuilder.CreateIndex(
                name: "IX_Cortes_EspecieId",
                table: "Cortes",
                column: "EspecieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cortes");

      

            migrationBuilder.AlterColumn<string>(
                name: "Etiqueta",
                table: "TroceriaDetalle",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)",
                oldMaxLength: 30);
        }
    }
}
