﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class boletosxchoferes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Chofer",
                table: "Boletos");

            migrationBuilder.RenameColumn(
                name: "Fecha",
                table: "Boletos",
                newName: "FechaSegundaPesada");

            migrationBuilder.AddColumn<int>(
                name: "ChoferId",
                table: "Boletos",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaPrimeraPesada",
                table: "Boletos",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Boletos_ChoferId",
                table: "Boletos",
                column: "ChoferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Boletos_Choferes_ChoferId",
                table: "Boletos",
                column: "ChoferId",
                principalTable: "Choferes",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Boletos_Choferes_ChoferId",
                table: "Boletos");

            migrationBuilder.DropIndex(
                name: "IX_Boletos_ChoferId",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "ChoferId",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "FechaPrimeraPesada",
                table: "Boletos");

            migrationBuilder.RenameColumn(
                name: "FechaSegundaPesada",
                table: "Boletos",
                newName: "Fecha");

            migrationBuilder.AddColumn<string>(
                name: "Chofer",
                table: "Boletos",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }
    }
}
