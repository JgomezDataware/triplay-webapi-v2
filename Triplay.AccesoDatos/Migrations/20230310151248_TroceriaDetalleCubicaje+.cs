﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class TroceriaDetalleCubicaje : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "cubicaje",
                table: "TroceriaDetalle",
                newName: "MetrosCubicos");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MetrosCubicos",
                table: "TroceriaDetalle",
                newName: "cubicaje");
        }
    }
}
