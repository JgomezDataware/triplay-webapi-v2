﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class guiaforetal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GuiaForestal",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "GuiaForestalRuta",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tarja",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TarjaRuta",
                table: "Boletos",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GuiaForestal",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "GuiaForestalRuta",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "Tarja",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "TarjaRuta",
                table: "Boletos");
        }
    }
}
