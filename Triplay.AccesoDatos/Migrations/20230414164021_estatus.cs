﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class estatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Estaus",
                table: "Troceria",
                newName: "Estatus");

            migrationBuilder.AddColumn<int>(
                name: "estatus",
                table: "TroceriaDetalle",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "estatus",
                table: "TroceriaDetalle");

            migrationBuilder.RenameColumn(
                name: "Estatus",
                table: "Troceria",
                newName: "Estaus");
        }
    }
}
