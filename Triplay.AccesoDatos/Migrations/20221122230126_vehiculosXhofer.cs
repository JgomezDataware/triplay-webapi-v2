﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class vehiculosXhofer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehiculos_Choferes_ChoferId",
                table: "Vehiculos");

            migrationBuilder.DropIndex(
                name: "IX_Vehiculos_ChoferId",
                table: "Vehiculos");

            migrationBuilder.DropColumn(
                name: "ChoferId",
                table: "Vehiculos");

            migrationBuilder.AddColumn<int>(
                name: "VehiculoId",
                table: "Choferes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Choferes_VehiculoId",
                table: "Choferes",
                column: "VehiculoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Choferes_Vehiculos_VehiculoId",
                table: "Choferes",
                column: "VehiculoId",
                principalTable: "Vehiculos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Choferes_Vehiculos_VehiculoId",
                table: "Choferes");

            migrationBuilder.DropIndex(
                name: "IX_Choferes_VehiculoId",
                table: "Choferes");

            migrationBuilder.DropColumn(
                name: "VehiculoId",
                table: "Choferes");

            migrationBuilder.AddColumn<int>(
                name: "ChoferId",
                table: "Vehiculos",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Vehiculos_ChoferId",
                table: "Vehiculos",
                column: "ChoferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehiculos_Choferes_ChoferId",
                table: "Vehiculos",
                column: "ChoferId",
                principalTable: "Choferes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
