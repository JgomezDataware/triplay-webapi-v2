﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class SP_Concentrado_Basico : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			var sp = @"CREATE PROCEDURE SP_CONCENTRADO_BASICO
	-- Add the parameters for the stored procedure here
	@date date
AS
BEGIN
Declare @especie varchar(50);
Declare  listEspecies Cursor
for 
Select Nombre from Especies;
open listEspecies;

drop table if exists TempIds
 select Id into TempIds from (
 select Troceria.id,Convert(date,Fecha) Fecha from Troceria
    inner join Boletos on BoletoId = Boletos.Id
	WHERE Troceria.Estatus = 1
   )  as T1

	where Fecha = @date
	create table TempTroncos(
	TAVM3 float,
	TTVM3 float,
	TTCM3 float,
	TABM3 float,
	TAMM3 float,
	TCDM3 float,
	TSBM3 float,
	TSMM3 float,
	TLNM3 float,
	Especie varchar(50)
	)
	
	FETCH next From listEspecies
	into @especie;
	while @@FETCH_STATUS = 0

	Begin
	INSERT INTO TempTroncos
	select  Round((SUM(AVM3)*(6/Power(0.3048000,3))),0) TAVM3, Round((SUM(TVM3)*(6/Power(0.3048000,3))),0) TTVM3,
	 Round((SUM(TCM3)*(6/Power(0.3048000,3))),0)TTCM3, Round((SUM(ABM3)*(6/Power(0.3048000,3))),10) TABN3, 
	 Round((SUM(AMM3)*(6/Power(0.3048000,3))),0)TAM3, Round((SUM(CDM3)*(6/Power(0.3048000,3))),0)TCDM3,
	 Round((SUM(SBM3)*(6/Power(0.3048000,3))),0)TSBM3, Round((SUM(SMM3)*(6/Power(0.3048000,3))),0) TSMM3,
	 Round((SUM(LNM3)*(6/Power(0.3048000,3))),0) TLNM3,@especie Especie
	from  ReporteTarjaview where id in  (select Id from TempIds)
	and Especie = @especie
	and TotalM3 is not null
	Group by Especie

	FETCH next From listEspecies
	into @especie;
	end
	close listEspecies;
	DEALLOCATE listEspecies;
	Select * from TempTroncos
	Drop Table TempTroncos
	END
";
			
			migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReporteTarjaview");
        }
    }
}
