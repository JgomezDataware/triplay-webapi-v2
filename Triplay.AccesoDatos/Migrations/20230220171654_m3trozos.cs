﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class m3trozos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Trozos",
                table: "Boletos",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "metroscubicos",
                table: "Boletos",
                type: "float",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Trozos",
                table: "Boletos");

            migrationBuilder.DropColumn(
                name: "metroscubicos",
                table: "Boletos");
        }
    }
}
