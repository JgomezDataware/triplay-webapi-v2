﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class clasificacionCortes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AB",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "AM",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "AV",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "CD",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "LN",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "SB",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "SM",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "TC",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "TV",
                table: "Cortes");

            migrationBuilder.AddColumn<string>(
                name: "Clasificacion",
                table: "Cortes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Descuento",
                table: "Cortes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Clasificacion",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "Descuento",
                table: "Cortes");

            migrationBuilder.AddColumn<double>(
                name: "AB",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "AM",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "AV",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "CD",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LN",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SB",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "SM",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TC",
                table: "Cortes",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TV",
                table: "Cortes",
                type: "float",
                nullable: true);
        }
    }
}
