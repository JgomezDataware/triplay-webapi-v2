﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class troceriaBoletos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Troceria_Guia",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "Guia",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "MetrosCubicos",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "Peso",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "Placas",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "Tarja",
                table: "Troceria");

            migrationBuilder.RenameColumn(
                name: "Trozos",
                table: "Troceria",
                newName: "BoletoId");

            migrationBuilder.CreateIndex(
                name: "IX_Troceria_BoletoId",
                table: "Troceria",
                column: "BoletoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Troceria_Boletos_BoletoId",
                table: "Troceria",
                column: "BoletoId",
                principalTable: "Boletos",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Troceria_Boletos_BoletoId",
                table: "Troceria");

            migrationBuilder.DropIndex(
                name: "IX_Troceria_BoletoId",
                table: "Troceria");

            migrationBuilder.RenameColumn(
                name: "BoletoId",
                table: "Troceria",
                newName: "Trozos");

            migrationBuilder.AddColumn<int>(
                name: "Guia",
                table: "Troceria",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "MetrosCubicos",
                table: "Troceria",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Peso",
                table: "Troceria",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Placas",
                table: "Troceria",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Tarja",
                table: "Troceria",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Troceria_Guia",
                table: "Troceria",
                column: "Guia",
                unique: true);
        }
    }
}
