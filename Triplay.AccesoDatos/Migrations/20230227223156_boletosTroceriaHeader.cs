﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class boletosTroceriaHeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pies",
                table: "Troceria");

            migrationBuilder.DropColumn(
                name: "PiesDoyle",
                table: "Troceria");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Pies",
                table: "Troceria",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PiesDoyle",
                table: "Troceria",
                type: "float",
                nullable: true);
        }
    }
}
