﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class ReporteGruas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			var sp = @"IF EXISTS 
	(SELECT specific_name 
	   FROM [INFORMATION_SCHEMA].[ROUTINES] 
	  WHERE routine_type = 'PROCEDURE' 
	    AND specific_schema = 'dbo' 
		AND specific_name = 'SP_Reporte_Gruas')
  BEGIN
	DROP PROCEDURE [dbo].[SP_Reporte_Gruas]
  END
GO

-- ====================================================
-- Author:		Alonso Sanchez
-- Create date: 
-- Description:	SP para obtener datos necesarios para crear reporte de gruas
-- History:
-- <14-04-2023> <Ja.Sanchez> Creacion de SP
-- ====================================================

CREATE PROCEDURE [dbo].[SP_Reporte_Gruas]

 

@FechaConsulta      VARCHAR(10) = '', -- MM/DD/YYYY
@FechaConsultaFin VARCHAR(10) = '' -- MM/DD/YYYY
AS
BEGIN
    DECLARE @RFiltrados TABLE(IdTarja INT, GA VARCHAR(10),GC VARCHAR(10),GAC VARCHAR(10),GAD VARCHAR(10),PIES FLOAT,GuiaForestal VARCHAR(10))
    IF(@FechaConsultaFin='')
    BEGIN
        INSERT INTO @RFiltrados SELECT T.Id, GA,GC,GAC,GAD,MetrosCubicosRecibidos* (6 / 3048 ^3) AS Pies,GuiaForestal FROM Troceria t
            INNER JOIN BOLETOS b
            ON t.BoletoId=b.Id
            WHERE CAST(Fecha AS DATE)= @FechaConsulta
    END
ELSE
    BEGIN 
        INSERT INTO @RFiltrados SELECT T.Id, GA,GC,GAC,GAD,MetrosCubicosRecibidos* (6 / 3048 ^3) AS Pies,GuiaForestal FROM Troceria t
            INNER JOIN BOLETOS b
            ON t.BoletoId=b.Id
            WHERE Fecha BETWEEN @FechaConsulta AND @FechaConsultaFin
    END 
   SELECT * FROM @RFiltrados
END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
