﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class cortexPadre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TroncoId",
                table: "Cortes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cortes_TroncoId",
                table: "Cortes",
                column: "TroncoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cortes_TroceriaDetalle_TroncoId",
                table: "Cortes",
                column: "TroncoId",
                principalTable: "TroceriaDetalle",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cortes_TroceriaDetalle_TroncoId",
                table: "Cortes");

            migrationBuilder.DropIndex(
                name: "IX_Cortes_TroncoId",
                table: "Cortes");

            migrationBuilder.DropColumn(
                name: "TroncoId",
                table: "Cortes");
        }
    }
}
