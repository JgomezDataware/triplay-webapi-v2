﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class checkimpresoras : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Troceria",
                table: "Impresoras",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Troceria",
                table: "Impresoras");
        }
    }
}
