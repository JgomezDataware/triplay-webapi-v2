﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class Troceria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Almacenes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Almacenes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Especies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Especies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Troceria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Guia = table.Column<int>(type: "int", nullable: false),
                    Tarja = table.Column<int>(type: "int", nullable: false),
                    Trozos = table.Column<int>(type: "int", nullable: false),
                    MetrosCubicos = table.Column<double>(type: "float", nullable: false),
                    Pies = table.Column<double>(type: "float", nullable: false),
                    PiesDoyle = table.Column<double>(type: "float", nullable: false),
                    Peso = table.Column<double>(type: "float", nullable: false),
                    Origen = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Placas = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Propietario = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OperadorId = table.Column<int>(type: "int", nullable: false),
                    GC = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GAC = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GA = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GAD = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Troceria", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TroceriaDetalle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fecha = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Turno = table.Column<int>(type: "int", nullable: false),
                    AlamcenId = table.Column<int>(type: "int", nullable: false),
                    EspecieId = table.Column<int>(type: "int", nullable: false),
                    D1 = table.Column<double>(type: "float", nullable: false),
                    D2 = table.Column<double>(type: "float", nullable: false),
                    LAR = table.Column<double>(type: "float", nullable: false),
                    DES = table.Column<double>(type: "float", nullable: false),
                    AV = table.Column<bool>(type: "bit", nullable: false),
                    TV = table.Column<bool>(type: "bit", nullable: false),
                    TC = table.Column<bool>(type: "bit", nullable: false),
                    AB = table.Column<bool>(type: "bit", nullable: false),
                    AM = table.Column<bool>(type: "bit", nullable: false),
                    CD = table.Column<bool>(type: "bit", nullable: false),
                    SB = table.Column<bool>(type: "bit", nullable: false),
                    SM = table.Column<bool>(type: "bit", nullable: false),
                    LN = table.Column<bool>(type: "bit", nullable: false),
                    CAB = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TroceriaDetalle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TroceriaDetalle_Almacenes_AlamcenId",
                        column: x => x.AlamcenId,
                        principalTable: "Almacenes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TroceriaDetalle_Especies_EspecieId",
                        column: x => x.EspecieId,
                        principalTable: "Especies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Troceria_OperadorId",
                table: "Troceria",
                column: "OperadorId");

            migrationBuilder.CreateIndex(
                name: "IX_TroceriaDetalle_AlamcenId",
                table: "TroceriaDetalle",
                column: "AlamcenId");

            migrationBuilder.CreateIndex(
                name: "IX_TroceriaDetalle_EspecieId",
                table: "TroceriaDetalle",
                column: "EspecieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Troceria");

            migrationBuilder.DropTable(
                name: "TroceriaDetalle");

            migrationBuilder.DropTable(
                name: "Almacenes");

            migrationBuilder.DropTable(
                name: "Especies");
        }
    }
}
