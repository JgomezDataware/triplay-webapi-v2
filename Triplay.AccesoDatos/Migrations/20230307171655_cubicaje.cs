﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Triplay.AccesoDatos.Migrations
{
    public partial class cubicaje : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "cubicaje",
                table: "TroceriaDetalle",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "pies",
                table: "TroceriaDetalle",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cubicaje",
                table: "TroceriaDetalle");

            migrationBuilder.DropColumn(
                name: "pies",
                table: "TroceriaDetalle");
        }
    }
}
