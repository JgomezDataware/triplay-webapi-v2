﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;


namespace Triplay.DataAccess
{
    public class TriplayContext : DbContext
    {
        public TriplayContext(DbContextOptions<TriplayContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Clasificacion>().HasData(
                new Clasificacion { Id = 1, Nombre = "TV", Tipo = Modelos.Enums.EClasificacion.TV },
                new Clasificacion { Id = 2, Nombre = "TC", Tipo = Modelos.Enums.EClasificacion.TC },
                new Clasificacion { Id = 3, Nombre = "AM", Tipo = Modelos.Enums.EClasificacion.AM },
                new Clasificacion { Id = 4, Nombre = "AB", Tipo = Modelos.Enums.EClasificacion.AB },
                new Clasificacion { Id = 5, Nombre = "AV", Tipo = Modelos.Enums.EClasificacion.AV },
                new Clasificacion { Id = 6, Nombre = "CD", Tipo = Modelos.Enums.EClasificacion.CD },
                new Clasificacion { Id = 7, Nombre = "LN", Tipo = Modelos.Enums.EClasificacion.LN },
                new Clasificacion { Id = 8, Nombre = "SB", Tipo = Modelos.Enums.EClasificacion.SB },
                new Clasificacion { Id = 9, Nombre = "SM", Tipo = Modelos.Enums.EClasificacion.SM }
            );
        }

        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Operadores> Operadores { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Boleto> Boletos { get; set; }
        public DbSet<Vehiculos> Vehiculos { get; set; }
        public DbSet<Chofer> Choferes { get; set; }
        public DbSet<TroceriaHeader> Troceria { get; set; }
        public DbSet<TroceriaDetalle> TroceriaDetalle { get; set; }
        public DbSet<Especie> Especies { get; set; }
        public DbSet<Almacen> Almacenes { get; set; }
        public DbSet<Impresora> Impresoras { get; set; }
        public DbSet<Gruas> Gruas { get; set; }
        public DbSet<Zonas> Zonas { get; set; }
        public DbSet<Cortes> Cortes { get; set; }
        public DbSet<Clasificacion> Clasificaciones { get; set; }

        #region
        //Custom Entities

        public DbSet<ReporteTarjaview> ReporteTarjaview { get; set; }

        public DbSet<ReporteGruas> ReporteGruas { get; set; }

        public DbSet<ConcentradoBasicoHeader> ConcentradoBasicoHeader { get; set; }

        public DbSet<ConcentradoBasicoDetalle> ConcentradoBasicoDetalle { get; set; }
        #endregion



    }
}
