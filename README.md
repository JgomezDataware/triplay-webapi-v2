# Desarrollo para Productora Triplay
Para Satisfacer las necesidades especificas de la operación del cliente
productora de Triplay se creo un API, en .NET Core 6.0 el siguiente readme pretente servir de guía para entender estructura y desarrollo del Proyecto
## Antecendentes
## Arquitectura
Para este Proyecto utilizamos una Arquitectura de Cebolla

![arquitectura](/Recursos/Imagenes/onion.PNG "Título alternativo")

La capa de Modelos Representada por el proyecto de mismo nombre y proyecto de Acceso a Datos

La capa de Repositorios por el proyecto del mismo nombre
usando también el patrón de unidad de Trabajo


## Bascula
El Módulo de Bascula esta pensado para Generar un comprobante de Recepción de Producto,Materia Prima y/o Insumos, asi como tomar evidencia (fotografias) y pesar los vehiculos que ingresan y salen de la Planta
### Camaras  
El Módulo de Bascula fue diseñado para trabajar con 4 cámaras IP, dichas IPs son configurables desde el archivo "appsettings.json" del proyecto 

En terminos un poco mas Tecnicos la autentificación hacia dichas Cámaras es a traves del protocolo DIGEST asi que si en un fututo cambia el Protocolo es necesario Reimplementar la interfaz IPCamarasService

## Troceria
El Módulo de Troceria fue pensado para recibir clasificar y etiquetar los "trozos" es responsive y esta adaptado para la terminal solicitada. 

Tambien este módulo esta conectado al modulo de Bascula de Manera Secuencial, es decir esto modulo va despues del modulo de boletos, 
Datos como el peso Neto, La Empresa, Elchofer entre otros se toman  a traves de un join 
## Comandos útiles
### Generar Migración 
desde  la ventana de Administrador de Paquetes y seleccionado el proyecto Acceso a Datos
~~~
add-migration {{nombre}}
~~~

### Crear o Actualizar la Base de Datos
~~~
update-database
~~~
## Despliegue
~~~
dotnet build
~~~
