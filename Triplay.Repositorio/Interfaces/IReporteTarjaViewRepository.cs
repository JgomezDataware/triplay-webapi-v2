﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface IReporteTarjaViewRepository 
    {
        IEnumerable<ReporteTarjaview> ObtenerTodos(Expression<Func<ReporteTarjaview, bool>> filter = null, Func<IQueryable<ReporteTarjaview>, IOrderedQueryable<ReporteTarjaview>> orderBy = null, string incluirPropiedades = null);
   

    }
}
