﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface IOperadorRepository:IRepositorioBase<Operadores>
    {
        void Update(Operadores operadores);
    }
}
