﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface ICortesRepository:IRepositorioBase<Cortes>
    {
        void Update(Cortes cortes);
        int count(Expression<Func<Cortes, bool>> filter = null);
    }
}
