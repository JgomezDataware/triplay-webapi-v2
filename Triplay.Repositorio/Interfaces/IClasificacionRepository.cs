﻿using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface IClasificacionRepository : IRepositorioBase<Clasificacion>
    {
    }
}
