﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.Migrations;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface IChoferRepository : IRepositorioBase<Chofer>
    {
        void Update(Chofer chofer);

        IEnumerable<Chofer> busqueda(string? nombre, string? placas, string? include);
    }
}
