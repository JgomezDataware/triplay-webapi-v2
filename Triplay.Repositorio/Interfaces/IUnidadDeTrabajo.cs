﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Repositorio.Interfaces;

namespace Triplay.Respositorio.Interfaces
{
    public interface IUnidadDeTrabajo : IDisposable
    {
        IEmpresaRepository empresaRepository { get; }
        IProudctoRepository productoRepository { get; }
        IOperadorRepository operadorRepository { get; }
        IVehiculoRepository vehiculoRepository { get; }
        IBoletoRepository boletoRepository { get; }
        IChoferRepository choferRepository { get; }
        IEspecieRespository especieRepository { get; }
        IAlmacenRepository almacenRepository { get; }
        IZonasRepository zonasRepository { get; }
        IGruasRespository gruasRepository { get; }
        ICortesRepository cortesRepository { get; }
        ITroceriaHeaderRepository troceriaHeaderRepository { get; }
        ITroceriaDetalleRespository troceriaDetalleRepository { get; }
        IImpresoraRepository impresoraRepository { get; }
        IReporteTarjaViewRepository reporteTarjaViewRepository {get;}
        IReporteGruasRepository reporteGruasRepository { get; }
        IClasificacionRepository clasificacionRepository { get; }
        

        void Guardar();
    }
}
