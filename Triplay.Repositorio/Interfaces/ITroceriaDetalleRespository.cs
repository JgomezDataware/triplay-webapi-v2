﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface ITroceriaDetalleRespository:IRepositorioBase<TroceriaDetalle>
    {
        void update(TroceriaDetalle respository);
        int count(Expression<Func<TroceriaDetalle, bool>> filter = null);
    }
}
