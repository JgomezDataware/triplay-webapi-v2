﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface ITroceriaHeaderRepository:IRepositorioBase<TroceriaHeader>
    {
        void Update(TroceriaHeader header);

        IEnumerable<TroceriaHeader> ObtenerUltimos(
            Expression<Func<TroceriaHeader, bool>> filter = null,
            Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null,
            string incluirPropiedades = ""
            );
    }
}
