﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Repositorio.Implementacion;

namespace Triplay.Repositorio.Interfaces
{
    public interface IReporteGruasRepository
    {
        IEnumerable<Grua> getDatosSP(string fechaIni,string fechaFin); 
    }
}
