﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Implementacion;
using Triplay.Respositorio.Interfaces;

namespace Triplay.Repositorio.Interfaces
{
    public interface IVehiculoRepository:IRepositorioBase<Vehiculos>
    {
        void Update(Vehiculos vehiculos);
        IEnumerable<Vehiculos> busqueda(string placas, string include = null);
    }
}
