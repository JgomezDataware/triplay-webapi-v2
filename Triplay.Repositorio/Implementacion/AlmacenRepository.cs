﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class AlmacenRepository:RepositorioBase<Almacen>,IAlmacenRepository
    {
        private readonly TriplayContext _context;
        public AlmacenRepository(TriplayContext context):base(context)
        {
            _context = context;
        }

        public void Update(Almacen almacen)
        {
            var alm = _context.Almacenes.FirstOrDefault(x=>x.Id ==almacen.Id);
            if(alm != null)
            {
                alm.Nombre = almacen.Nombre;
                _context.Almacenes.Update(alm);
                
            }

        }
    }
}
