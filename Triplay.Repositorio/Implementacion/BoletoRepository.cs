﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class BoletoRepository : RepositorioBase<Boleto>, IBoletoRepository
    {
        private readonly TriplayContext _context;
        public BoletoRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }
        public void Update(Boleto boleto)
        {
            var boletoviejo = _context.Boletos.FirstOrDefault(x => x.Id == boleto.Id);
            if (boletoviejo != null)
            {
                boletoviejo.EmpresaId = boleto.EmpresaId;
                boletoviejo.ProductoId = boleto.ProductoId;
                boletoviejo.PrimerOperador = boleto.PrimerOperador;
                boletoviejo.SegundoOperador = boleto.SegundoOperador;
                boletoviejo.ChoferId = boleto.ChoferId;
                boletoviejo.Placas = boleto.Placas;
                boletoviejo.PrimeraPesada = boleto.PrimeraPesada;
                boletoviejo.SegundaPesada = boleto.SegundaPesada;
                boletoviejo.FechaPrimeraPesada = boleto.FechaPrimeraPesada;
                boletoviejo.FechaSegundaPesada = boleto.FechaSegundaPesada;
                boletoviejo.PesoNeto = boleto.PesoNeto;
                boletoviejo.PrimerTipo = boleto.PrimerTipo;
                boletoviejo.SegundoTipo = boleto.SegundoTipo;

                boletoviejo.CapturasPrimeraPesada = boleto.CapturasPrimeraPesada;
                boletoviejo.CapturasSegundaPesada = boleto.CapturasSegundaPesada;
                boletoviejo.GuiaForestal = boleto.GuiaForestal;
                boletoviejo.GuiaForestalRuta = boleto.GuiaForestalRuta;
                boletoviejo.Tarja = boleto.Tarja;
                boletoviejo.TarjaRuta = boleto.TarjaRuta;
                boletoviejo.metroscubicos = boleto.metroscubicos;
                boletoviejo.Trozos = boleto.Trozos;
                boletoviejo.FolioConsecutivo = boleto.FolioConsecutivo;
                boletoviejo.Almacen = boleto.Almacen;
                boletoviejo.Estatus = boleto.Estatus;
                boletoviejo.FolioConsecutivo = boleto.FolioConsecutivo;





                _context.Boletos.Update(boletoviejo);

            }




        }
    }
}
