﻿
using Triplay.Respositorio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Repositorio.Interfaces;
using Triplay.Repositorio.Implementacion;

namespace Triplay.Respositorio.Implementacion
{
    public class UnidadDeTrabajo : IUnidadDeTrabajo
    {
        private readonly TriplayContext _context;
        public IEmpresaRepository empresaRepository { get; private set; }
        public IProudctoRepository productoRepository { get; private set; }
        public IOperadorRepository operadorRepository { get; private set; }
        public IVehiculoRepository vehiculoRepository { get; private set; }
        public IBoletoRepository boletoRepository { get; private set; }
        public IChoferRepository choferRepository { get; private set; }
        public IEspecieRespository especieRepository { get; private set; }
        public IAlmacenRepository almacenRepository { get; private set; }
        public IZonasRepository zonasRepository { get; private set; }
        public IGruasRespository gruasRepository { get; private set; }
        public ICortesRepository cortesRepository { get; private set; }
        public ITroceriaHeaderRepository troceriaHeaderRepository { get; private set; }
        public ITroceriaDetalleRespository troceriaDetalleRepository { get; private set; }
        public IImpresoraRepository impresoraRepository { get; private set; }
        public IReporteTarjaViewRepository reporteTarjaViewRepository { get; private set; }
        public IReporteGruasRepository reporteGruasRepository { get; private set; }
        public IClasificacionRepository clasificacionRepository { get; private set; }

        public UnidadDeTrabajo(TriplayContext context)
        {
            _context = context;
            empresaRepository = new EmpresaRepository(_context);
            productoRepository = new ProductoRepository(_context);
            operadorRepository = new OperadorRepository(_context);
            vehiculoRepository = new VehiculoRepository(_context);
            boletoRepository = new BoletoRepository(_context);
            choferRepository = new ChoferRepository(_context);
            especieRepository = new EspeciesRepository(_context);
            almacenRepository = new AlmacenRepository(_context);
            zonasRepository = new ZonasRepository(_context);
            cortesRepository = new CortesRepository(_context);
            troceriaHeaderRepository = new TroceriaHeaderRepository(_context);
            gruasRepository = new GruasRepository(_context);
            troceriaDetalleRepository = new TroceriaDetalleRepository(_context);
            impresoraRepository = new ImpresoraRepository(_context);
            reporteTarjaViewRepository = new ReporteTarjeViewRepository(_context);
            reporteGruasRepository = new ReporteGruasRepository(_context);
            clasificacionRepository = new ClasificacionRepository(_context);
        }

        public void Guardar()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
