﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class TroceriaDetalleRepository : RepositorioBase<TroceriaDetalle>, ITroceriaDetalleRespository
    {
        private readonly TriplayContext _context;
        public TroceriaDetalleRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        /*public int count(Expression<Func<TroceriaDetalle, bool>> filter = null)
        {
            IQueryable<TroceriaDetalle> query = dbSet;
            query = query.Where(filter);
            return query.Count();
        }*/

        public void update(TroceriaDetalle respository)
        {
            var data = _context.TroceriaDetalle.FirstOrDefault(x => x.Id == respository.Id);
            if (data != null)
            {
                data.EspecieId = respository.EspecieId;
                data.D1 = respository.D1;
                data.D2 = respository.D2;
                data.LAR = respository.LAR;
                data.DES = respository.DES;
                data.AV = respository.AV;
                data.TV = respository.TV;
                data.TC = respository.TC;
                data.AB = respository.AB;
                data.AM = respository.AM;
                data.CD = respository.CD;
                data.SB = respository.SB;
                data.SM = respository.SM;
                data.LN = respository.LN;
                data.CAB = respository.CAB;
                data.MetrosCubicos = respository.MetrosCubicos;
                data.pies = respository.pies;
                data.Etiqueta = respository.Etiqueta;
                data.estatus = respository.estatus;
                _context.TroceriaDetalle.Update(data);
            }



        }
    }
}
