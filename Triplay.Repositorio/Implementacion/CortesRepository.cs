﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class CortesRepository : RepositorioBase<Cortes>, ICortesRepository
    {
        private readonly TriplayContext _context;
        public CortesRepository(TriplayContext context):base(context)
        {
            _context = context;
        }

        public void Update(Cortes cortes)
        {
            var corte = _context.Cortes.FirstOrDefault(x=> x.Id == cortes.Id);
            if (corte != null)
            {
                corte.D1 = cortes.D1;
                corte.D2 = cortes.D2;
                corte.LAR = cortes.LAR;
                corte.EspecieId = cortes.EspecieId;
                /*corte.AV = cortes.AV;
                corte.TV = cortes.TV;
                corte.TC = cortes.TC;
                corte.AB = cortes.AB;
                corte.AM = cortes.AM;
                corte.CD = cortes.CD;
                corte.SB = cortes.SB;
                corte.SM = cortes.SM;
                corte.LN = cortes.LN;*/
                corte.Etiqueta = cortes.Etiqueta;
                corte.Color = cortes.Color;
                corte.TroncoId = cortes.TroncoId;
            }
        }

    }
}
