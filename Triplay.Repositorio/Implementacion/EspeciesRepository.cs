﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class EspeciesRepository:RepositorioBase<Especie>,IEspecieRespository
    {
        private readonly TriplayContext _context;
        public EspeciesRepository(TriplayContext context):base (context)
        {
            _context = context;
        }

        public void Update(Especie especie)
        {
            var esp = _context.Especies.FirstOrDefault(x => x.Id == especie.Id);
            if(esp != null)
            {
                esp.Nombre = especie.Nombre;
                esp.Codigo = especie.Codigo;
                _context.Especies.Update(esp);
            }

        }
        public IEnumerable<Especie> busqueda(string codigo)
        {
            return _context.Especies.Where(c => c.Codigo.Contains(codigo)).ToList();
        }
    }
}
