﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ReporteTarjeViewRepository : IReporteTarjaViewRepository
    {

        private readonly TriplayContext _context;
        public ReporteTarjeViewRepository(TriplayContext db) 
        {
            _context = db;
        }

        public IEnumerable<ReporteTarjaview> ObtenerTodos(Expression<Func<ReporteTarjaview, bool>> filter = null, Func<IQueryable<ReporteTarjaview>, IOrderedQueryable<ReporteTarjaview>> orderBy = null, string incluirPropiedades = null)
        {
            IQueryable<ReporteTarjaview> query = _context.ReporteTarjaview;

            if (filter != null)
            {
                query = query.Where(filter);   // select * from where ...
            }

            if (incluirPropiedades != null)
            {
                foreach (var incluirProp in incluirPropiedades.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(incluirProp);
                }
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }

            return query.ToList();

        }
    }
}
