﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ChoferRepository : RepositorioBase<Chofer>, IChoferRepository
    {
        private readonly TriplayContext _context;
        public ChoferRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public void Update(Chofer chofer)
        {
            var cho = _context.Choferes.FirstOrDefault(x => x.Id == chofer.Id);
            if (cho != null)
            {
                cho.Nombre = chofer.Nombre;
                cho.VehiculoId = chofer.VehiculoId;
                _context.Choferes.Update(cho);
            }

        }
        public IEnumerable<Chofer> busqueda(string nombre, string? placas, string include)
        {
            if (placas == null)
                return _context.Choferes.Where(c => c.Nombre.Contains(nombre)).Include(include).ToList();
            else
                return _context.Choferes.Where(c => c.Vehiculos.Placas.Equals(placas)).Include(include).ToList();

        }
    }
}
