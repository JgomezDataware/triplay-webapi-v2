﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class OperadorRepository : RepositorioBase<Operadores>, IOperadorRepository
    {
        private readonly TriplayContext _context;
        public OperadorRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public void Update(Operadores operadores)
        {
            var oper = _context.Operadores.FirstOrDefault(x => x.Id == operadores.Id);
            if (oper != null)
            {
                oper.Nombre = operadores.Nombre;
                oper.Tipo_Usuario = operadores.Tipo_Usuario;
                oper.Acceso = operadores.Acceso;
                oper.Turno = operadores.Turno;
                _context.Operadores.Update(oper);
            }
        }
    }
}
