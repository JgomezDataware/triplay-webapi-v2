﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ImpresoraRepository : RepositorioBase<Impresora>, IImpresoraRepository
    {
        private readonly TriplayContext _context;

        public ImpresoraRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public void Update(Impresora impresora)
        {
            var imp = _context.Impresoras.FirstOrDefault(x => x.Id == impresora.Id);
            if (imp != null)
            {
                imp.Descripcion = impresora.Descripcion;
                imp.DireccionPort = impresora.DireccionPort;
                imp.DireccionIp = impresora.DireccionIp;
                imp.Troceria = impresora.Troceria;
                _context.Impresoras.Update(imp);
            }
        }
    }
}
