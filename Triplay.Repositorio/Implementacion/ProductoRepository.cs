﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ProductoRepository : RepositorioBase<Producto>, IProudctoRepository
    {
        private readonly TriplayContext _context;
        public ProductoRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public void Update(Producto producto)
        {
            var prod = _context.Productos.FirstOrDefault(x => x.Id == producto.Id);
            if (prod != null)
            {
                prod.Nombre = producto.Nombre;
                _context.Productos.Update(prod);
            }
        }
        public IEnumerable<Producto> busqueda(string nombre)
        {
            return _context.Productos.Where(c => c.Nombre.Contains(nombre)).ToList();
        }
    }
}
