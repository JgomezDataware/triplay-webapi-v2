﻿using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ClasificacionRepository :RepositorioBase<Clasificacion>, IClasificacionRepository
    {
    private readonly TriplayContext _context;

        public ClasificacionRepository(TriplayContext context):base(context)
        {
            _context = context;
        }
    }
}
