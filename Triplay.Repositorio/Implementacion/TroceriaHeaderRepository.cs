﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class TroceriaHeaderRepository:RepositorioBase<TroceriaHeader>, ITroceriaHeaderRepository
    {
        private readonly TriplayContext _context;
        public TroceriaHeaderRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<TroceriaHeader> ObtenerUltimos(Expression<Func<TroceriaHeader, bool>> filter = null,
            Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null, string incluirPropiedades = "")
        {
            IQueryable<TroceriaHeader> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);   // select * from where ...
            }

            if (incluirPropiedades != null)
            {
                foreach (var incluirProp in incluirPropiedades.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(incluirProp);
                }
            }

            if (orderBy != null)
            {
                return orderBy(query).Take(5).ToList();
            }

            return query.Take(5).ToList();
        }

        public void Update(TroceriaHeader header)
        {
            var data = _context.Troceria.FirstOrDefault(x => x.Id == header.Id);
            if(data!= null)
            {
                data.Id = header.Id;
                data.Fecha = header.Fecha;
                data.Origen = header.Origen;
                data.Propietario = header.Propietario;
                data.OperadorId = header.OperadorId;
                data.BoletoId = header.BoletoId;
                data.TrozosRecibidos = header.TrozosRecibidos;
                data.Cabezas = header.Cabezas;
                data.MetrosCubicosRecibidos = header.MetrosCubicosRecibidos;
                data.Factor = header.Factor;
                data.DiferenciaTrozos = header.DiferenciaTrozos;
                data.DiferenciaMetros = header.DiferenciaMetros;
                data.GC = header.GC;
                data.GAC = header.GAC;
                data.GA = header.GA;
                data.GAD = header.GAD;
                data.Estatus = header.Estatus;

                _context.Update(data);

            }
        }

      
    }
}
