﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.AccesoDatos.DTOs;

namespace Triplay.Repositorio.Implementacion
{
    public class ReporteGruasRepository : IReporteGruasRepository
    {
        private readonly TriplayContext _context;
        public ReporteGruasRepository(TriplayContext db)
        {
            _context = db;
        }

        public IEnumerable<Grua> getDatosSP(string fechaIni, string fechaFin)
        //MM/DD/YYYY
        {
            Grua grua = new Grua();
            List<Grua> gruas = new List<Grua>();
            var tarjas = _context.ReporteGruas.FromSqlRaw("SP_Reporte_Gruas @FechaConsulta,@FechaConsultaFin",
                new SqlParameter("@FechaConsulta", fechaIni),
                new SqlParameter("@FechaConsultaFin", fechaFin)
                );
            foreach (var tarja in tarjas)
            {
                if (tarja.GAC != null)
                {
                    gruas.Add(new Grua() { Codigo = tarja.GAC, GuiaForestal = tarja.GuiaForestal, Pies = tarja.Pies, TipoTrabajo = "GAC" });
                }
                if (tarja.GAD != null)
                {
                    gruas.Add(new Grua() { Codigo = tarja.GAD, GuiaForestal = tarja.GuiaForestal, Pies = tarja.Pies, TipoTrabajo = "GAD" });
                }
                if (tarja.GC != null)
                {
                    gruas.Add(new Grua() { Codigo = tarja.GC, GuiaForestal = tarja.GuiaForestal, Pies = tarja.Pies, TipoTrabajo = "GC" });
                }
                if (tarja.GA != null)
                {
                    gruas.Add(new Grua() { Codigo = tarja.GA, GuiaForestal = tarja.GuiaForestal, Pies = tarja.Pies, TipoTrabajo = "GA" });
                }

            }
            return gruas;
        }
    }

}
