﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class ZonasRepository : RepositorioBase<Zonas>, IZonasRepository
    {
        private readonly TriplayContext _context;


        public ZonasRepository(TriplayContext context):base(context)
        {
            _context = context;
        }
        public void update(Zonas zonas)
        {
            var zona = _context.Zonas.FirstOrDefault(x => x.Id == zonas.Id);
              if(zona != null)
            {
                zona.Nombre = zonas.Nombre;
                _context.Zonas.Update(zona);
            }

        }
    }
}
