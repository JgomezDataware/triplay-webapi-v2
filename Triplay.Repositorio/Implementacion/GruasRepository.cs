﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class GruasRepository:RepositorioBase<Gruas>,IGruasRespository
    {
        private readonly TriplayContext _context;
        public GruasRepository(TriplayContext context): base(context)
        {
            _context = context;
        }

        public void Update(Gruas gruas)
        {
            var data = _context.Gruas.FirstOrDefault(x => x.Id == gruas.Id);
            if(data != null)
            {
                data.Nombre = gruas.Nombre;
                data.Codigo = gruas.Codigo;
                data.CardCode = gruas.CardCode;
                _context.Update(data);
            }

        }
    }
}
