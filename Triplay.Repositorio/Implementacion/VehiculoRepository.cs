﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Triplay.Repositorio.Implementacion
{
    public class VehiculoRepository : RepositorioBase<Vehiculos>, IVehiculoRepository
    {
        private readonly TriplayContext _context;

        public VehiculoRepository(TriplayContext context) : base(context)
        {
            _context = context;
        }

        public void Update(Vehiculos vehiculos)
        {
            var vehi = _context.Vehiculos.FirstOrDefault(x => x.Id == vehiculos.Id);
            if (vehi != null)
            {
                vehi.Fecha = vehiculos.Fecha;
                vehi.EmpresaId = vehiculos.EmpresaId;
                vehi.Placas = vehiculos.Placas;
                vehi.OperadorId = vehiculos.OperadorId;
                _context.Vehiculos.Update(vehi);
            }
        }
        public IEnumerable<Vehiculos> busqueda(string placa, string incluirPropiedades = null)
        {
            IQueryable<Vehiculos> query = dbSet;

            if (incluirPropiedades != null)
            {
                foreach (var incluirProp in incluirPropiedades.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(incluirProp);
                }
            }

            query = query.Where(v => v.Placas.Contains(placa));   // select * from where ...
            return query.ToList();
        }

    }
}
