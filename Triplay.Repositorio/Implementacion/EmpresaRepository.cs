﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.DataAccess;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Implementacion;

namespace Triplay.Repositorio.Implementacion
{
    public class EmpresaRepository:RepositorioBase<Empresa>, IEmpresaRepository
    {
        private readonly TriplayContext _context;
        public EmpresaRepository(TriplayContext context):base(context)
        {
            _context = context;
        }

        public void Update(Empresa empresa)
        {
            var emp = _context.Empresas.FirstOrDefault(x=> x.Id == empresa.Id);
            if(emp != null)
            {
                emp.Nombre = empresa.Nombre;
                emp.Categoria = empresa.Categoria;
                _context.Empresas.Update(emp);
            }
        }
    }
}
