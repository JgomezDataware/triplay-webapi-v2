﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IAlmacenesSL
    {
        Task<ServiceLayerResponse> GetAlmacenes(); 
     
    }
}
