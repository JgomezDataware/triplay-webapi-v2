﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IClasificacionService
    {
        Clasificacion Get(int id);
        IEnumerable<Clasificacion> GetAll(Expression<Func<Clasificacion, bool>> filter = null,
         Func<IQueryable<Clasificacion>, IOrderedQueryable<Clasificacion>> orderBy = null,
            string include = null);
        Clasificacion GetFirst(
         Expression<Func<Clasificacion, bool>> filter = null,
         string include = null
        );
    }
}
