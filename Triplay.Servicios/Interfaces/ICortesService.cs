﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface ICortesService
    {
        Cortes Get(int id);
        IEnumerable<Cortes> GetAll(Expression<Func<Cortes, bool>> filter = null,
         Func<IQueryable<Cortes>, IOrderedQueryable<Cortes>> orderBy = null,
            string include = null);
        Cortes GetFirst(
         Expression<Func<Cortes, bool>> filter = null,
         string include = null
        );

        void Add(Cortes entity);

        void Update(Cortes entity);


        void Delete(int id);
        void Delete(Cortes entity);

        void DeleteMany(IEnumerable<Cortes> entity);

        bool IsValid(string etiqueta);
        bool IsTroncoValid(string etiqueta);
        bool IsTarjaValid(string etiqueta);
        int count(Expression<Func<Cortes, bool>> filter = null);
        Cortes convertClas(Cortes corte, CortesDTO corteDto);

    }
}
