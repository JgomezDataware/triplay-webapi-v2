﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IBusssinesPartner
    {
        Task<ServiceLayerResponse> GetExtraccionista();
        Task<ServiceLayerResponse> GetFletero();

        Task<ServiceLayerResponse> PostBussinesPartner(businessPartnerSLDTO businessPartnerSL);
       

    }
}
