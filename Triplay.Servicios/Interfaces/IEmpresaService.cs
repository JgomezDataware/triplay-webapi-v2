﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;


namespace Triplay.Services.Interfaces
{
    public interface IEmpresaService
    {
        Empresa Get(int id);
       IEnumerable<Empresa> GetAll(Expression<Func<Empresa, bool>> filter = null,
        Func<IQueryable<Empresa>, IOrderedQueryable<Empresa>> orderBy = null,
           string include = null);
        Empresa GetFirst(
         Expression<Func<Empresa, bool>> filter = null,
         string include = null
        );

        void Add(Empresa entity);

       void Update(Empresa entity);
        void Delete(int id);
        void Delete(Empresa entity);

        void DeleteMany(IEnumerable<Empresa> entity);
    }
}
