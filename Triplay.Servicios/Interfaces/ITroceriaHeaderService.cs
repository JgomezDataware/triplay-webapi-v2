﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface ITroceriaHeaderService
    {
        TroceriaHeader Get(int id);
        IEnumerable<TroceriaHeader> GetAll(Expression<Func<TroceriaHeader, bool>> filter = null,
         Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null,
            string include = null);

        IEnumerable<TroceriaHeader> GetLast(Expression<Func<TroceriaHeader, bool>> filter = null,
         Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null,
            string include = null);

        TroceriaHeader GetFirst(
         Expression<Func<TroceriaHeader, bool>> filter = null,
         string include = null
        );

        void Add(TroceriaHeader entity);

        void Update(TroceriaHeader entity);
        void CerrarTarja(int id);
        
        void Delete(int id);
        void Delete(TroceriaHeader entity);

        void DeleteMany(IEnumerable<TroceriaHeader> entity);
    }
}
