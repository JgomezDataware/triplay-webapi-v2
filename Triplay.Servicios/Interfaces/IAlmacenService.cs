﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IAlmacenService
    {
        Almacen Get(int id);
        IEnumerable<Almacen> GetAll(Expression<Func<Almacen, bool>> filter = null,
         Func<IQueryable<Almacen>, IOrderedQueryable<Almacen>> orderBy = null,
            string include = null);
        Almacen GetFirst(
         Expression<Func<Almacen, bool>> filter = null,
         string include = null
        );

        void Add(Almacen entity);

        void Update(Almacen entity);


        void Delete(int id);
        void Delete(Almacen entity);

        void DeleteMany(IEnumerable<Almacen> entity);
    }
}