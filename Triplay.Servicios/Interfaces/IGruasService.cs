﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IGruasService
    {
        Gruas Get(int id);
        IEnumerable<Gruas> GetAll(Expression<Func<Gruas, bool>> filter = null,
         Func<IQueryable<Gruas>, IOrderedQueryable<Gruas>> orderBy = null,
            string include = null);
        Gruas GetFirst(
         Expression<Func<Gruas, bool>> filter = null,
         string include = null
        );

        void Add(Gruas entity);

        void Update(Gruas entity);

        void Delete(int id);
        void Delete(Gruas entity);

        void DeleteMany(IEnumerable<Gruas> entity);
    }
}
