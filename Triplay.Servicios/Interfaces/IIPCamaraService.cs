﻿using Triplay.AccesoDatos.DTOs;

namespace Triplay.Servicios.Interfaces
{
    public interface IIPCamaraService
    {
        Task<IEnumerable<CapturaCamaraIPDTO>> ObtenerCapturas();
    }
}
