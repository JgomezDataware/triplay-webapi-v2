﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IReporteGruasService
    {
        IEnumerable<Grua> GetDatosSP(string fechaIni, string fechaFin);
    }
}
