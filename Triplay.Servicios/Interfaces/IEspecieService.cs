﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IEspecieService
    {
        Especie Get(int id);
        IEnumerable<Especie> GetAll(Expression<Func<Especie, bool>> filter = null,
         Func<IQueryable<Especie>, IOrderedQueryable<Especie>> orderBy = null,
            string include = null);
        Especie GetFirst(
         Expression<Func<Especie, bool>> filter = null,
         string include = null
        );

        void Add(Especie entity);

        void Update(Especie entity);

        void Delete(int id);
        void Delete(Especie entity);

        void DeleteMany(IEnumerable<Especie> entity);

        IEnumerable<Especie> busqueda(string codigo);
    }
}
