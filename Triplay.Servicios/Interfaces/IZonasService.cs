﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IZonasService
    {
        Zonas Get(int id);
        
        IEnumerable<Zonas> GetAll(Expression<Func<Zonas, bool>> filter = null,
         Func<IQueryable<Zonas>, IOrderedQueryable<Zonas>> orderBy = null,
            string include = null);
        Zonas GetFirst(
         Expression<Func<Zonas, bool>> filter = null,
         string include = null
        );

        void Add(Zonas entity);

        void Update(Zonas entity);


        void Delete(int id);
        void Delete(Zonas entity);

        void DeleteMany(IEnumerable<Zonas> entity);
    }
}
