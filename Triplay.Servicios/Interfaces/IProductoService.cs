﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IProductoService
    {
        Producto Get(int id);
        IEnumerable<Producto> GetAll(Expression<Func<Producto, bool>> filter = null,
         Func<IQueryable<Producto>, IOrderedQueryable<Producto>> orderBy = null,
            string include = null);
        Producto GetFirst(
         Expression<Func<Producto, bool>> filter = null,
         string include = null
        );

        void Add(Producto entity);

        void Update(Producto producto);
        void Delete(int id);
        void Delete(Producto entity);
        void DeleteMany(IEnumerable<Producto> entity);
        IEnumerable<Producto> busqueda(string nombre);
    }
}
