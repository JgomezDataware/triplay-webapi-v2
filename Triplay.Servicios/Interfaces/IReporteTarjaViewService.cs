﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IReporteTarjaViewService
    {


        IEnumerable<ReporteTarjaview> GetAll(Expression<Func<ReporteTarjaview, bool>> filter = null,
         Func<IQueryable<ReporteTarjaview>, IOrderedQueryable<ReporteTarjaview>> orderBy = null,
            string include = null);

       
    }
}
