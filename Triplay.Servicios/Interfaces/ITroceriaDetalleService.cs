﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface ITroceriaDetalleService
    {
        TroceriaDetalle Get(int id);
        IEnumerable<TroceriaDetalle> GetAll(Expression<Func<TroceriaDetalle, bool>> filter = null,
         Func<IQueryable<TroceriaDetalle>, IOrderedQueryable<TroceriaDetalle>> orderBy = null,
            string include = null);

        TroceriaDetalle GetFirst(
         Expression<Func<TroceriaDetalle, bool>> filter = null,
         string include = null
        );

        void Add(TroceriaDetalle entity);

        void Update(TroceriaDetalle entity);
        void Delete(int id);

        void Delete(TroceriaDetalle entity);

        void DeleteMany(IEnumerable<TroceriaDetalle> entity);

        bool sum(TroceriaDetalle entity);

        int count(Expression<Func<TroceriaDetalle, bool>> filter = null);
    }
}
