﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IBoletoService
    {
        Boleto Get(int id);
        IEnumerable<Boleto> GetAll(Expression<Func<Boleto, bool>> filter = null,
         Func<IQueryable<Boleto>, IOrderedQueryable<Boleto>> orderBy = null,
            string include = null);

        Boleto GetOne(Expression<Func<Boleto, bool>> filter = null,
        Func<IQueryable<Boleto>, IOrderedQueryable<Boleto>> orderBy = null,
           string include = null);

        Boleto GetFirst(
         Expression<Func<Boleto, bool>> filter = null,
         string include = null
        );

        void Add(Boleto entity);

        void Update(Boleto entity);

        void Delete(int id);
        void Delete(Boleto entity);

        void DeleteMany(IEnumerable<Boleto> entity);

        IEnumerable<Boleto> filtro(DateTime? fecha, string? placas, string? chofer, string? cardCode);
    }
}
