﻿namespace Triplay.Servicios.Interfaces
{
    public interface IServiceLayerAuth
    {

          Task<string> Login();
          Task Logout();
    }
}
