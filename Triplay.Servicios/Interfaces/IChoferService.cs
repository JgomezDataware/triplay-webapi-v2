﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IChoferService
    {
        Chofer Get(int id);
        IEnumerable<Chofer> GetAll(Expression<Func<Chofer, bool>> filter = null,
         Func<IQueryable<Chofer>, IOrderedQueryable<Chofer>> orderBy = null,
            string include = null);
        Chofer GetFirst(
         Expression<Func<Chofer, bool>> filter = null,
         string filtertype = null,
         string include = null
        );

        void Add(Chofer entity);

        void Update(Chofer entity);
        void Delete(int id);
        void Delete(Chofer entity);

        void DeleteMany(IEnumerable<Chofer> entity);

        IEnumerable<Chofer> busqueda(string? nombre, string? placas,
            string include = null);
    }
}
