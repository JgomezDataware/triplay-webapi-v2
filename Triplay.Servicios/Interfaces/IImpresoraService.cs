﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Services.Interfaces
{
    public interface IImpresoraService
    {
        Impresora Get(int id);
        IEnumerable<Impresora> GetAll(Expression<Func<Impresora, bool>> filter = null,
       Func<IQueryable<Impresora>, IOrderedQueryable<Impresora>> orderBy = null,
          string include = null);
        Impresora GetFirst(
         Expression<Func<Impresora, bool>> filter = null,
         string include = null
        );

        void Add(Impresora entity);

        void Update(Impresora entity);
        void Delete(int id);
        void Delete(Impresora entity);
    }
}
