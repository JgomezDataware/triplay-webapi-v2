﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IOperadorService
    {
        Operadores Get(int id);
        IEnumerable<Operadores> GetAll(Expression<Func<Operadores, bool>> filter = null,
         Func<IQueryable<Operadores>, IOrderedQueryable<Operadores>> orderBy = null,
            string include = null);
        Operadores GetFirst(
         Expression<Func<Operadores, bool>> filter = null,
         string include = null
        );

        void Add(Operadores entity);

        void Update(Operadores entity);
        void Delete(int id);
        void Delete(Operadores entity);

        void DeleteMany(IEnumerable<Operadores> entity);
    }
}
