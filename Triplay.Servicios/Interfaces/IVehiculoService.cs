﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public interface IVehiculoService
    {
        Vehiculos Get(int id);
        IEnumerable<Vehiculos> GetAll(Expression<Func<Vehiculos, bool>> filter = null,
         Func<IQueryable<Vehiculos>, IOrderedQueryable<Vehiculos>> orderBy = null,
            string include = null);
        Vehiculos GetFirst(
         Expression<Func<Vehiculos, bool>> filter = null,
         string include = null
        );

        void Add(Vehiculos entity);

        void Update(Vehiculos entity);

        void Delete(int id);
        void Delete(Vehiculos entity);

        void DeleteMany(IEnumerable<Vehiculos> entity);
        IEnumerable<Vehiculos> busqueda(string placas, string include = null);
    }
}
