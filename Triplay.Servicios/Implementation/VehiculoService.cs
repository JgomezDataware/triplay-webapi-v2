﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class VehiculoService:IVehiculoService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public VehiculoService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Vehiculos entity)
        {
            _unitOfWork.vehiculoRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }
        public void Update(Vehiculos entity)
        {
            _unitOfWork.vehiculoRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
            _unitOfWork.vehiculoRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Vehiculos entity)
        {
            _unitOfWork.vehiculoRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Vehiculos> entity)
        {
            _unitOfWork.vehiculoRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Vehiculos Get(int id)
        {

            return _unitOfWork.vehiculoRepository.Obtener(id);
        }
        public IEnumerable<Vehiculos> GetAll(Expression<Func<Vehiculos, bool>> filter = null, Func<IQueryable<Vehiculos>, IOrderedQueryable<Vehiculos>> orderBy = null, string include = null)
        {
            return _unitOfWork.vehiculoRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Vehiculos GetFirst(Expression<Func<Vehiculos, bool>> filter = null, string include = null)
        {
            return _unitOfWork.vehiculoRepository.ObtenerPrimero(filter, include);
        }
        public IEnumerable<Vehiculos> busqueda(string placas, string include = null)
        {
            return _unitOfWork.vehiculoRepository.busqueda(placas, include);
        }
    }
}
