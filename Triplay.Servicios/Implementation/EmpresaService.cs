﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Services.Interfaces;

namespace Triplay.Services.Implementation
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public EmpresaService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public  void Add(Empresa entity)
        {
          _unitOfWork.empresaRepository.Agregar(entity);
             _unitOfWork.Guardar();
        }

        public void Update(Empresa entity)
        {
            _unitOfWork.empresaRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
             _unitOfWork.empresaRepository.Remover(id);
             _unitOfWork.Guardar();
        }

        public void Delete(Empresa entity)
        {
            _unitOfWork.empresaRepository.Remover(entity);
             _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Empresa> entity)
        {
            _unitOfWork.empresaRepository.RemoverRango(entity);
           _unitOfWork.Guardar();
        }

        public Empresa Get(int id)
        {

            return  _unitOfWork.empresaRepository.Obtener(id);
        }
        public IEnumerable<Empresa> GetAll(Expression<Func<Empresa, bool>> filter = null, Func<IQueryable<Empresa>, IOrderedQueryable<Empresa>> orderBy = null, string include = null)
        {
            return  _unitOfWork.empresaRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Empresa GetFirst(Expression<Func<Empresa, bool>> filter = null, string include = null)
        {
            return  _unitOfWork.empresaRepository.ObtenerPrimero(filter, include);
        }

    }
}
