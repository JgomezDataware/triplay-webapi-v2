﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Modelos.Enums;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class CortesService : ICortesService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public CortesService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Cortes entity)
        {
            if (entity.Clasificacion == EClasificacion.TC || entity.Clasificacion == EClasificacion.TV)
            {
                entity.Color = EColor.Rojo;
            }
            else if (entity.Clasificacion == EClasificacion.AB)
            {
                entity.Color = EColor.Verde;
            }
            else
            {
                entity.Color = EColor.Blanco;
            }
            entity.Descuento = entity.Descuento != null ? entity.Descuento : 0;
            _unitOfWork.cortesRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.cortesRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Cortes entity)
        {
            _unitOfWork.cortesRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Cortes> entity)
        {
            _unitOfWork.cortesRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Cortes almacen)
        {
            _unitOfWork.cortesRepository.Update(almacen);
            _unitOfWork.Guardar();
        }
        public Cortes Get(int id)
        {

            return _unitOfWork.cortesRepository.Obtener(id);
        }
        public IEnumerable<Cortes> GetAll(Expression<Func<Cortes, bool>> filter = null, Func<IQueryable<Cortes>, IOrderedQueryable<Cortes>> orderBy = null, string include = null)
        {
            return _unitOfWork.cortesRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Cortes GetFirst(Expression<Func<Cortes, bool>> filter = null, string include = null)
        {
            return _unitOfWork.cortesRepository.ObtenerPrimero(filter, include);
        }

        public bool IsValid(string etiqueta)
        {
            return (IsTroncoValid(etiqueta) && IsTarjaValid(etiqueta));
        }

        public bool IsTroncoValid(string etiqueta)
        {
            var tronco = _unitOfWork.troceriaDetalleRepository.ObtenerPrimero(x => x.Etiqueta == etiqueta);
            if (tronco == null)
            {
                return false;
            }
            if (tronco.estatus == EETronco.DISPONIBLE)
            {
                tronco.estatus = EETronco.CORTE;
                _unitOfWork.troceriaDetalleRepository.update(tronco);
                _unitOfWork.Guardar();
            }
            return (tronco.estatus == EETronco.DISPONIBLE || tronco.estatus == EETronco.CORTE);
        }
        public bool IsTarjaValid(string etiqueta)
        {
            var tronco = _unitOfWork.troceriaDetalleRepository.ObtenerPrimero(x => x.Etiqueta == etiqueta);
            var tarja = _unitOfWork.troceriaHeaderRepository.Obtener(tronco.TroceriaId);
            if (tarja.Estatus == EEstatus.COMPLETO || tarja.Estatus == EEstatus.CERRADO)
            {
                if (tarja.Estatus == EEstatus.COMPLETO)
                {
                    tarja.Estatus = EEstatus.CERRADO;
                    _unitOfWork.troceriaHeaderRepository.Update(tarja);
                    _unitOfWork.Guardar();

                }
            }
            return (tarja.Estatus == EEstatus.COMPLETO || tarja.Estatus == EEstatus.CERRADO);
        }

        public int count(Expression<Func<Cortes, bool>> filter = null)
        {
            return _unitOfWork.cortesRepository.count(filter);
        }

        public Cortes convertClas(Cortes cortemap, CortesDTO cortedto)
        {
            /* cortemap.AB = cortedto.Clasificacion == EClasificacion.AB ? cortedto.LAR : 0;
             cortemap.AV = cortedto.Clasificacion == EClasificacion.AV ? cortedto.LAR : 0;
             cortemap.CD = cortedto.Clasificacion == EClasificacion.CD ? cortedto.LAR : 0;
             cortemap.AM = cortedto.Clasificacion == EClasificacion.AM ? cortedto.LAR : 0;
             cortemap.TV = cortedto.Clasificacion == EClasificacion.TV ? cortedto.LAR : 0;
             cortemap.CD = cortedto.Clasificacion == EClasificacion.CD ? cortedto.LAR : 0;
             cortemap.TC = cortedto.Clasificacion == EClasificacion.TC ? cortedto.LAR : 0;
             cortemap.SB = cortedto.Clasificacion == EClasificacion.SB ? cortedto.LAR : 0;
             cortemap.LN = cortedto.Clasificacion == EClasificacion.LN ? cortedto.LAR : 0;*/
            return cortemap;
        }
    }

}
