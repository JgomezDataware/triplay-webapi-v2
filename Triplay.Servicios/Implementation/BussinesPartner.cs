﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class BussinesPartner : IBusssinesPartner
    {
        private readonly IServiceLayerAuth _serviceLayerAuth;
        private readonly IConfiguration _configuration;
        public BussinesPartner(IServiceLayerAuth serviceLayerAuth, IConfiguration configuration)
        {
            _serviceLayerAuth = serviceLayerAuth;
            _configuration = configuration;
        }

        public async Task<ServiceLayerResponse> GetFletero()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Cookie", $"B1SESSION={await _serviceLayerAuth.Login()}; Path=/b1s/v1; HttpOnly;");
                var urlBase = new Uri(_configuration.GetSection("AppSettings:BaseAddress").Value);
                var response = await client.GetAsync(urlBase + "BusinessPartners?$select=CardCode,CardName&$filter=Properties2 eq 'tYES'");
                _serviceLayerAuth.Logout();
                var data = JsonSerializer.Deserialize<ServiceLayerResponse>(await response.Content.ReadAsStringAsync());
                return data;
            }
        }

        public async Task<ServiceLayerResponse> GetExtraccionista()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Cookie", $"B1SESSION={await _serviceLayerAuth.Login()}; Path=/b1s/v1; HttpOnly;");
                var urlBase = new Uri(_configuration.GetSection("AppSettings:BaseAddress").Value);
                var response = await client.GetAsync(urlBase + "BusinessPartners?$select=CardCode,CardName&$filter=Properties1 eq 'tYES'");
                _serviceLayerAuth.Logout();
                var data = JsonSerializer.Deserialize<ServiceLayerResponse>(await response.Content.ReadAsStringAsync());
                return data;
            }
        }

        public async Task<ServiceLayerResponse> PostBussinesPartner(businessPartnerSLDTO businessPartnerSL)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Cookie", $"B1SESSION={await _serviceLayerAuth.Login()}; Path=/b1s/v1; HttpOnly;");
                var urlBase = new Uri(_configuration.GetSection("AppSettings:BaseAddress").Value);

                var request = JsonSerializer.Serialize<businessPartnerSLDTO>(businessPartnerSL);
                var response = await client.PostAsJsonAsync(urlBase + "BusinessPartners", businessPartnerSL, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
                _serviceLayerAuth.Logout();
               
                var data = JsonSerializer.Deserialize<ServiceLayerResponse>(await response.Content.ReadAsStringAsync());
                return data;
            }

        }
    }
}
