﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ReporteTarjaviewServivce :IReporteTarjaViewService
    {
        private readonly IUnidadDeTrabajo _unidadDeTrabajo;

        public ReporteTarjaviewServivce(IUnidadDeTrabajo unidadDeTrabajo)
        {
            _unidadDeTrabajo = unidadDeTrabajo;
        }

        public IEnumerable<ReporteTarjaview> GetAll(Expression<Func<ReporteTarjaview, bool>> filter = null, Func<IQueryable<ReporteTarjaview>, IOrderedQueryable<ReporteTarjaview>> orderBy = null, string include = null)
        {
            return _unidadDeTrabajo.reporteTarjaViewRepository.ObtenerTodos(filter, orderBy, include);
        }
    }
}
