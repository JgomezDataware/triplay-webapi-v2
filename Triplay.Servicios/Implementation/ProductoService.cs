﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ProductoService :IProductoService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public ProductoService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(Producto entity)
        {
            _unitOfWork.productoRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Producto producto)
        {
            _unitOfWork.productoRepository.Update(producto);
            _unitOfWork.Guardar();

        }
        public void Delete(int id)
        {
            _unitOfWork.productoRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Producto entity)
        {
            _unitOfWork.productoRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Producto> entity)
        {
            _unitOfWork.productoRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Producto Get(int id)
        {

            return _unitOfWork.productoRepository.Obtener(id);
        }
        public IEnumerable<Producto> GetAll(Expression<Func<Producto, bool>> filter = null, Func<IQueryable<Producto>, IOrderedQueryable<Producto>> orderBy = null, string include = null)
        {
            return _unitOfWork.productoRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Producto GetFirst(Expression<Func<Producto, bool>> filter = null, string include = null)
        {
            return _unitOfWork.productoRepository.ObtenerPrimero(filter, include);
        }
        public IEnumerable<Producto> busqueda(string nombre)
        {
            return _unitOfWork.productoRepository.busqueda(nombre);
        }
    }
}
