﻿using System.Linq.Expressions;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class BoletoService : IBoletoService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public BoletoService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(Boleto entity)
        {
            _unitOfWork.boletoRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.boletoRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Boleto entity)
        {
            _unitOfWork.boletoRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Boleto> entity)
        {
            _unitOfWork.boletoRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Boleto Get(int id)
        {
            return _unitOfWork.boletoRepository.Obtener(id);
        }
        public IEnumerable<Boleto> GetAll(Expression<Func<Boleto, bool>> filter = null, Func<IQueryable<Boleto>, IOrderedQueryable<Boleto>> orderBy = null, string include = null)
        {
            return _unitOfWork.boletoRepository.ObtenerTodos(filter, orderBy, include);
        }

        public Boleto GetOne(Expression<Func<Boleto, bool>> filter = null, Func<IQueryable<Boleto>, IOrderedQueryable<Boleto>> orderBy = null, string include = null)
        {
            return _unitOfWork.boletoRepository.ObtenerUltimo(filter, orderBy, include);
        }

        public Boleto GetFirst(Expression<Func<Boleto, bool>> filter = null, string include = null)
        {
            return _unitOfWork.boletoRepository.ObtenerPrimero(filter, include);
        }

        public void Update(Boleto boleto)
        {
            _unitOfWork.boletoRepository.Update(boleto);
            _unitOfWork.Guardar();
        }

        public IEnumerable<Boleto> filtro(DateTime? fecha, string? placas, string? chofer, string? cardCode)
        {
            Expression<Func<Boleto, bool>> filter = null;
            Func<IQueryable<Boleto>, IOrderedQueryable<Boleto>> orderBy = x => x.OrderByDescending(x => x.FechaPrimeraPesada);
            string include = "Empresa,Producto,Chofer";

            if (fecha == null)
                if (placas == null)
                    if (cardCode == null)
                        if (chofer == null)
                            return Enumerable.Empty<Boleto>();
                        else
                            filter = x => x.Chofer.Nombre == chofer;
                    else if (chofer == null)
                        filter = x =>x.Almacen == cardCode;
                    else
                        filter = x => x.Chofer.Nombre == chofer && x.Almacen == cardCode;
                else if (cardCode == null)
                    if (chofer == null)
                        filter = x => x.Placas == placas;
                    else
                        filter = x => x.Chofer.Nombre == chofer && x.Placas == placas;
                else if (chofer == null)
                    filter = x => x.Almacen == cardCode && x.Placas == placas;
                else
                    filter = x => x.Chofer.Nombre == chofer && x.Almacen == cardCode && x.Placas == placas;
            else
                if (placas == null)
                if (cardCode == null)
                    if (chofer == null)
                        filter = x => x.FechaPrimeraPesada.Date == fecha;
                    else
                        filter = x => x.Chofer.Nombre == chofer && x.FechaPrimeraPesada.Date == fecha;
                else if (chofer == null)
                    filter = x => x.FechaPrimeraPesada.Date == fecha && x.Almacen == cardCode;
                else
                    filter = x => x.Chofer.Nombre == chofer && x.FechaPrimeraPesada.Date == fecha && x.Almacen == cardCode;
            else if (cardCode == null)
                if (chofer == null)
                    filter = x => x.FechaPrimeraPesada.Date == fecha && x.Placas == placas;
                else
                    filter = x => x.Chofer.Nombre == chofer && x.FechaPrimeraPesada.Date == fecha && x.Placas == placas;
            else if (chofer == null)
                filter = x => x.FechaPrimeraPesada.Date == fecha && x.Almacen == cardCode && x.Placas == placas;
            else
                filter = x => x.Chofer.Nombre == chofer && x.FechaPrimeraPesada.Date == fecha && x.Almacen == cardCode && x.Placas == placas;

            return _unitOfWork.boletoRepository.ObtenerTodos(filter, orderBy, include);
        }
    }
}
