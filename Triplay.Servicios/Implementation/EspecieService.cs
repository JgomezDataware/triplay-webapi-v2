﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class EspecieService:IEspecieService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public EspecieService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Especie entity)
        {
            _unitOfWork.especieRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }
        public void Update(Especie entity){
            _unitOfWork.especieRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
            _unitOfWork.especieRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Especie entity)
        {
            _unitOfWork.especieRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Especie> entity)
        {
            _unitOfWork.especieRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Especie Get(int id)
        {

            return _unitOfWork.especieRepository.Obtener(id);
        }
        public IEnumerable<Especie> GetAll(Expression<Func<Especie, bool>> filter = null, Func<IQueryable<Especie>, IOrderedQueryable<Especie>> orderBy = null, string include = null)
        {
            return _unitOfWork.especieRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Especie GetFirst(Expression<Func<Especie, bool>> filter = null, string include = null)
        {
            return _unitOfWork.especieRepository.ObtenerPrimero(filter, include);
        }

        public IEnumerable<Especie> busqueda(string codigo)
        {
            return _unitOfWork.especieRepository.busqueda(codigo);
        }
    }
}
