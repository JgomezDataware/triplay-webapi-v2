﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class AlmacenService:IAlmacenService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public AlmacenService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Almacen entity)
        {
            _unitOfWork.almacenRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.almacenRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Almacen entity)
        {
            _unitOfWork.almacenRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Almacen> entity)
        {
            _unitOfWork.almacenRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Almacen almacen)
        {
            _unitOfWork.almacenRepository.Update(almacen);
            _unitOfWork.Guardar();
        }
        public Almacen Get(int id)
        {

            return _unitOfWork.almacenRepository.Obtener(id);
        }
        public IEnumerable<Almacen> GetAll(Expression<Func<Almacen, bool>> filter = null, Func<IQueryable<Almacen>, IOrderedQueryable<Almacen>> orderBy = null, string include = null)
        {
            return _unitOfWork.almacenRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Almacen GetFirst(Expression<Func<Almacen, bool>> filter = null, string include = null)
        {
            return _unitOfWork.almacenRepository.ObtenerPrimero(filter, include);
        }
    }
}
