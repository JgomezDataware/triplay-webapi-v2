﻿
using Microsoft.Extensions.Configuration;
using System.Net.Http.Json;
using System.Text.Json;
using Triplay.Modelos;

namespace Triplay.Servicios.Interfaces
{
    public class ServiceLayerAuth : IServiceLayerAuth
    {
        private readonly HttpClient _httpClient;
        private readonly Login _login;
        private readonly IConfiguration _configuration;

        public ServiceLayerAuth(IConfiguration configuration)
        {
            _configuration = configuration;   
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(_configuration.GetSection("AppSettings:BaseAddress").Value);
            _login = new Login
            {
                CompanyDB = _configuration.GetSection("AppSettings:CompanyDB").Value,
                Password = _configuration.GetSection("AppSettings:Password").Value,
                UserName = _configuration.GetSection("AppSettings:UserName").Value

            };

        }

        public async Task<string> Login()
        {
            var response = await _httpClient.PostAsJsonAsync<Login>("Login", _login, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
            try
            {
                var session = JsonSerializer.Deserialize<Session>(await response.Content.ReadAsStringAsync());
                return session.SessionId;

            }
            catch (Exception ex)
            {
                return "fallo XD";
            }

          
        }

        public async Task Logout()
        {
            await _httpClient.PostAsync("http://192.168.10.45:50001/b1s/v1/Logout", null);
        }
    }
}
