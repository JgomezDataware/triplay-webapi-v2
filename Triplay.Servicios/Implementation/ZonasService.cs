﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ZonasService:IZonasService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;
        public ZonasService(IUnidadDeTrabajo unitofWork)
        {
            _unitOfWork = unitofWork;
        }


        public void Add(Zonas entity)
        {
            _unitOfWork.zonasRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.zonasRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Zonas entity)
        {
            _unitOfWork.zonasRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Zonas> entity)
        {
            _unitOfWork.zonasRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Zonas Zonas)
        {
            _unitOfWork.zonasRepository.update(Zonas);
            _unitOfWork.Guardar();
        }
        public Zonas Get(int id)
        {

            return _unitOfWork.zonasRepository.Obtener(id);
        }
        public IEnumerable<Zonas> GetAll(Expression<Func<Zonas, bool>> filter = null, Func<IQueryable<Zonas>, IOrderedQueryable<Zonas>> orderBy = null, string include = null)
        {
            return _unitOfWork.zonasRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Zonas GetFirst(Expression<Func<Zonas, bool>> filter = null, string include = null)
        {
            return _unitOfWork.zonasRepository.ObtenerPrimero(filter, include);
        }
    }
}
