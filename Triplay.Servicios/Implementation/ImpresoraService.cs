﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Services.Interfaces;

namespace Triplay.Services.Implementation
{
    public class ImpresoraService:IImpresoraService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public ImpresoraService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(Impresora entity)
        {
            _unitOfWork.impresoraRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Impresora entity)
        {
            _unitOfWork.impresoraRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
            _unitOfWork.impresoraRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Impresora entity)
        {
            _unitOfWork.impresoraRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public Impresora Get(int id)
        {

            return _unitOfWork.impresoraRepository.Obtener(id);
        }
        public IEnumerable<Impresora> GetAll(Expression<Func<Impresora, bool>> filter = null, Func<IQueryable<Impresora>, IOrderedQueryable<Impresora>> orderBy = null, string include = null)
        {
            return _unitOfWork.impresoraRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Impresora GetFirst(Expression<Func<Impresora, bool>> filter = null, string include = null)
        {
            return _unitOfWork.impresoraRepository.ObtenerPrimero(filter, include);
        }

    }
}
