﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class AlmacenesSL : IAlmacenesSL
    {
        private readonly IServiceLayerAuth _serviceLayerAuth;
        private readonly IConfiguration _configuration;
        public AlmacenesSL(IServiceLayerAuth serviceLayerAuth, IConfiguration configuration)
        {
            _serviceLayerAuth = serviceLayerAuth;
            _configuration = configuration;
        }
        public async Task<ServiceLayerResponse> GetAlmacenes()
        {
            using (var client = new HttpClient())
            {
                try
                {


                    client.DefaultRequestHeaders.Add("Cookie", $"B1SESSION={await _serviceLayerAuth.Login()}; Path=/b1s/v1; HttpOnly;");
                    var urlBase = new Uri(_configuration.GetSection("AppSettings:BaseAddress").Value);
                    var response = await client.GetAsync(urlBase + "Warehouses?$select=WarehouseCode,WarehouseName&$filter=U_DTW_PORTALIA eq 'Y'");
                    _serviceLayerAuth.Logout();
                    var data = JsonSerializer.Deserialize<ServiceLayerResponse>(await response.Content.ReadAsStringAsync());
                    return data;
                }
                catch(Exception ex)
                {
                    return new ServiceLayerResponse();
                }

            }

        }
    }
}
