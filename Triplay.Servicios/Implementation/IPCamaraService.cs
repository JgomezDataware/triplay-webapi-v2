﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.Extensions.Options;
using System.IO;
using System.Net;
using System.Threading.Channels;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class IPCamaraService : IIPCamaraService
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IpCamaraSettigs ipCamaraSettigs;

        public IPCamaraService(IHostingEnvironment hostingEnvironment, IOptions<IpCamaraSettigs> options)
        {
            _hostingEnvironment = hostingEnvironment;
            ipCamaraSettigs = options.Value;
        }

        public async Task<IEnumerable<CapturaCamaraIPDTO>> ObtenerCapturas()
        {
            var result = new List<CapturaCamaraIPDTO>();
            //var path = Path.Combine(_hostingEnvironment.WebRootPath, ipCamaraSettigs.SnapshotPath);
            //if(!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}




            IEnumerable<Task<byte[]>> tasks = Enumerable.Empty<Task<byte[]>>();
            foreach (var cam in ipCamaraSettigs.CamSettings)
                {
                tasks = tasks.Append(descargarImagen(cam));
                }
                var imagenes = await Task.WhenAll(tasks);
                foreach (var imgBytes in imagenes)
                {
                    var capturaDto = new CapturaCamaraIPDTO();

                    if (imgBytes.Length > 0)
                    {
                        var nombreImagen = $"{DateTime.Now.ToFileTimeUtc()}.jpg";
                        bool folderExists = Directory.Exists(ipCamaraSettigs.SnapshotPath + $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}");
                         if (!folderExists) Directory.CreateDirectory(ipCamaraSettigs.SnapshotPath + $"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}");

                    var rutaGuardar = Path.Combine(ipCamaraSettigs.SnapshotPath+$"/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}", nombreImagen);
                        await File.WriteAllBytesAsync(rutaGuardar, imgBytes);

                        capturaDto.NombreImagen = nombreImagen;
                        capturaDto.UrlImagen = $"DTW_CapturasCamarasIP/{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day}/{nombreImagen}";
                    }
                    else
                    {
                        capturaDto.NombreImagen = "error.png";
                        capturaDto.UrlImagen = "error.png";
                    }

                    result.Add(capturaDto);
                }
            

            return result;
        }

        public async Task<byte[]> descargarImagen(CamSettings cam)
        {
            try
            {
                
                var credencialCache = new CredentialCache();
                credencialCache.Add(new Uri(cam.url),
                    ipCamaraSettigs.AuthType,
                    new NetworkCredential(cam.user, cam.password));

                var httpHandler = new HttpClientHandler
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                    Credentials = credencialCache
                };

               var httpClient = new HttpClient(httpHandler);    
              
                return await httpClient.GetByteArrayAsync(cam.url);
            } catch (Exception ex)
            {
               
                return new byte[] { };
            }
        }
    }
}
