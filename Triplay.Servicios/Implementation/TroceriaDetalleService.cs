﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class TroceriaDetalleService : ITroceriaDetalleService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;
        private const double factor = 0.3048;
        private const double factorpies = 0.211888;
        private const double pi = 3.1416;
        public TroceriaDetalleService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(TroceriaDetalle entity)
        {
            var exist = _unitOfWork.troceriaDetalleRepository.count(x => x.TroceriaId == entity.TroceriaId);
            if (exist != 0)
            {
                var tronco = _unitOfWork.troceriaDetalleRepository.ObtenerUltimo(x => x.TroceriaId == entity.TroceriaId, x => x.OrderByDescending(t => t.Id));
                entity.Consecutivo = tronco.Consecutivo + 1;
            }
            else
            {
                entity.Consecutivo = 1;
            }
            entity.MetrosCubicos = ((pi * entity.LAR * factor) / 4 * Math.Pow((entity.D1 + entity.D2) / 200, 2)) * (1 - entity.DES / 100);
            entity.pies = entity.MetrosCubicos * factorpies;
            entity.MetrosCubicos = Math.Round(entity.MetrosCubicos.Value, 3, MidpointRounding.AwayFromZero);
            entity.pies = Math.Round(entity.pies.Value, 2, MidpointRounding.AwayFromZero);


            _unitOfWork.troceriaDetalleRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.troceriaDetalleRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Update(TroceriaDetalle entity)
        {

            /**/
            entity.MetrosCubicos = ((pi * entity.LAR * factor) / 4 * Math.Pow((entity.D1 + entity.D2) / 200, 2)) * (1 - entity.DES / 100);
            entity.pies = entity.MetrosCubicos * factorpies;
            entity.MetrosCubicos = Math.Round(entity.MetrosCubicos.Value, 3, MidpointRounding.AwayFromZero);
            entity.pies = Math.Round(entity.pies.Value, 2, MidpointRounding.AwayFromZero);
            _unitOfWork.troceriaDetalleRepository.update(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(TroceriaDetalle entity)
        {
            _unitOfWork.troceriaDetalleRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<TroceriaDetalle> entity)
        {
            _unitOfWork.troceriaDetalleRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public TroceriaDetalle Get(int id)
        {

            return _unitOfWork.troceriaDetalleRepository.Obtener(id);
        }
        public IEnumerable<TroceriaDetalle> GetAll(Expression<Func<TroceriaDetalle, bool>> filter = null, Func<IQueryable<TroceriaDetalle>, IOrderedQueryable<TroceriaDetalle>> orderBy = null, string include = null)
        {
            return _unitOfWork.troceriaDetalleRepository.ObtenerTodos(filter, orderBy, include);
        }
        public TroceriaDetalle GetFirst(Expression<Func<TroceriaDetalle, bool>> filter = null, string include = null)
        {
            return _unitOfWork.troceriaDetalleRepository.ObtenerPrimero(filter, include);
        }

        public bool sum(TroceriaDetalle entity)
        {
            entity.TV = entity.TV != null ? entity.TV : 0;
            entity.AV = entity.AV != null ? entity.AV : 0;
            entity.TC = entity.TC != null ? entity.TC : 0;
            entity.AB = entity.AB != null ? entity.AB : 0;
            entity.AM = entity.AM != null ? entity.AM : 0;
            entity.CD = entity.CD != null ? entity.CD : 0;
            entity.SB = entity.SB != null ? entity.SB : 0;
            entity.SM = entity.SM != null ? entity.SM : 0;
            entity.LN = entity.LN != null ? entity.LN : 0;

            double suma = entity.AB.Value + entity.AM.Value + entity.AV.Value + entity.CD.Value
            + entity.LN.Value + entity.SB.Value + entity.SM.Value + entity.TC.Value + entity.TV.Value;
            if (suma == entity.LAR)
                return true;
            return false;
        }

        public int count(Expression<Func<TroceriaDetalle, bool>> filter = null)
        {
            return _unitOfWork.troceriaDetalleRepository.count(filter);
        }
    }
}
