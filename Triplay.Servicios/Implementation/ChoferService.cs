﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ChoferService : IChoferService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public ChoferService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Chofer entity)
        {
            _unitOfWork.choferRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }
        public void Update(Chofer entity)
        {
            _unitOfWork.choferRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
            _unitOfWork.choferRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Chofer entity)
        {
            _unitOfWork.choferRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Chofer> entity)
        {
            _unitOfWork.choferRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Chofer Get(int id)
        {

            return _unitOfWork.choferRepository.Obtener(id);
        }
        public IEnumerable<Chofer> GetAll(Expression<Func<Chofer, bool>> filter = null, Func<IQueryable<Chofer>, IOrderedQueryable<Chofer>> orderBy = null, string include = null)
        {
            return _unitOfWork.choferRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Chofer GetFirst(Expression<Func<Chofer, bool>> filter = null, string filtertype = null, string include = null)
        {
            return _unitOfWork.choferRepository.ObtenerPrimero(filter, include);
        }

        public IEnumerable<Chofer> busqueda(string nombre, string placas, string include = null)
        {
            return _unitOfWork.choferRepository.busqueda(nombre, placas, include);
        }
    }
}
