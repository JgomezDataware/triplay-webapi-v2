﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ClasificacionService: IClasificacionService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public ClasificacionService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Clasificacion Get(int id)
        {
            return _unitOfWork.clasificacionRepository.Obtener(id);
        }
        public IEnumerable<Clasificacion> GetAll(Expression<Func<Clasificacion, bool>> filter = null, Func<IQueryable<Clasificacion>, IOrderedQueryable<Clasificacion>> orderBy = null, string include = null)
        {
            return _unitOfWork.clasificacionRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Clasificacion GetFirst(Expression<Func<Clasificacion, bool>> filter = null, string include = null)
        {
            return _unitOfWork.clasificacionRepository.ObtenerPrimero(filter, include);
        }

    }
}
