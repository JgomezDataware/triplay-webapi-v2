﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class OperadorService:IOperadorService
    {

        private readonly IUnidadDeTrabajo _unitOfWork;

        public OperadorService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Operadores entity)
        {
            _unitOfWork.operadorRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Update(Operadores entity)
        {
            _unitOfWork.operadorRepository.Update(entity);
            _unitOfWork.Guardar();

        }

        public void Delete(int id)
        {
            _unitOfWork.operadorRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Operadores entity)
        {
            _unitOfWork.operadorRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Operadores> entity)
        {
            _unitOfWork.operadorRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Operadores Get(int id)
        {

            return _unitOfWork.operadorRepository.Obtener(id);
        }
        public IEnumerable<Operadores> GetAll(Expression<Func<Operadores, bool>> filter = null, Func<IQueryable<Operadores>, IOrderedQueryable<Operadores>> orderBy = null, string include = null)
        {
            return _unitOfWork.operadorRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Operadores GetFirst(Expression<Func<Operadores, bool>> filter = null, string include = null)
        {
            return _unitOfWork.operadorRepository.ObtenerPrimero(filter, include);
        }
    }
}
