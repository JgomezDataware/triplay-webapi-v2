﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class GruasService:IGruasService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public GruasService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(Gruas entity)
        {
            _unitOfWork.gruasRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }
        public void Update(Gruas entity)
        {
            _unitOfWork.gruasRepository.Update(entity);
            _unitOfWork.Guardar();
        }
        public void Delete(int id)
        {
            _unitOfWork.gruasRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(Gruas entity)
        {
            _unitOfWork.gruasRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<Gruas> entity)
        {
            _unitOfWork.gruasRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public Gruas Get(int id)
        {

            return _unitOfWork.gruasRepository.Obtener(id);
        }
        public IEnumerable<Gruas> GetAll(Expression<Func<Gruas, bool>> filter = null, Func<IQueryable<Gruas>, IOrderedQueryable<Gruas>> orderBy = null, string include = null)
        {
            return _unitOfWork.gruasRepository.ObtenerTodos(filter, orderBy, include);
        }
        public Gruas GetFirst(Expression<Func<Gruas, bool>> filter = null, string include = null)
        {
            return _unitOfWork.gruasRepository.ObtenerPrimero(filter, include);
        }
    }
}
