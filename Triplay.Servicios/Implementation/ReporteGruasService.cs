﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.AccesoDatos.DTOs;
using Triplay.Modelos;
using Triplay.Repositorio.Interfaces;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class ReporteGruasService : IReporteGruasService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public ReporteGruasService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Grua> GetDatosSP(string fechaIni, string fechaFin)
        {
            return _unitOfWork.reporteGruasRepository.getDatosSP(fechaIni, fechaFin);
        }

    }
}
