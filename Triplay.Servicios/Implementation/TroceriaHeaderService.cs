﻿using IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Modelos.Enums;
using Triplay.Respositorio.Interfaces;
using Triplay.Servicios.Interfaces;

namespace Triplay.Servicios.Implementation
{
    public class TroceriaHeaderService : ITroceriaHeaderService
    {
        private readonly IUnidadDeTrabajo _unitOfWork;

        public TroceriaHeaderService(IUnidadDeTrabajo unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public void Add(TroceriaHeader entity)
        {

            _unitOfWork.troceriaHeaderRepository.Agregar(entity);
            _unitOfWork.Guardar();
        }

        public void Update(TroceriaHeader entity)
        {
            _unitOfWork.troceriaHeaderRepository.Update(entity);
            _unitOfWork.Guardar();
        }

        public void Delete(int id)
        {
            _unitOfWork.troceriaHeaderRepository.Remover(id);
            _unitOfWork.Guardar();
        }

        public void Delete(TroceriaHeader entity)
        {
            _unitOfWork.troceriaHeaderRepository.Remover(entity);
            _unitOfWork.Guardar();
        }

        public void DeleteMany(IEnumerable<TroceriaHeader> entity)
        {
            _unitOfWork.troceriaHeaderRepository.RemoverRango(entity);
            _unitOfWork.Guardar();
        }

        public TroceriaHeader Get(int id)
        {


            return _unitOfWork.troceriaHeaderRepository.Obtener(id);
        }
        public IEnumerable<TroceriaHeader> GetAll(Expression<Func<TroceriaHeader, bool>> filter = null, Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null, string include = null)
        {
            return _unitOfWork.troceriaHeaderRepository.ObtenerTodos(filter, orderBy, include);
        }
        public IEnumerable<TroceriaHeader> GetLast(Expression<Func<TroceriaHeader, bool>> filter = null, Func<IQueryable<TroceriaHeader>, IOrderedQueryable<TroceriaHeader>> orderBy = null, string include = null)
        {
            return _unitOfWork.troceriaHeaderRepository.ObtenerUltimos(filter, orderBy, include);
        }
        public TroceriaHeader GetFirst(Expression<Func<TroceriaHeader, bool>> filter = null, string include = null)
        {
            return _unitOfWork.troceriaHeaderRepository.ObtenerPrimero(filter, include);
        }


        public void CerrarTarja(int id)
        {

            
            var tarja = _unitOfWork.troceriaHeaderRepository.ObtenerPrimero(x=>x.Id==id,incluirPropiedades: "Boleto");
            
            tarja.TrozosRecibidos= _unitOfWork.troceriaDetalleRepository.ObtenerTodos(x => x.TroceriaId == tarja.Id).ToList().Count;
            if (tarja.TrozosRecibidos >= 1 && tarja.Boleto?.Estatus==EEstatus.CERRADO)
            {


                tarja.MetrosCubicosRecibidos = _unitOfWork.troceriaDetalleRepository.ObtenerTodos(x => x.TroceriaId == tarja.Id).Sum(s => s.MetrosCubicos);
                tarja.Cabezas = _unitOfWork.troceriaDetalleRepository.ObtenerTodos(x => x.TroceriaId == tarja.Id).ToList().Count(x => x.CAB == true);
                tarja.DiferenciaTrozos = tarja.Boleto.Trozos - tarja.TrozosRecibidos;
                tarja.DiferenciaMetros = tarja.MetrosCubicosRecibidos - tarja.Boleto.metroscubicos;
                tarja.Factor = tarja.Boleto.PesoNeto / tarja.MetrosCubicosRecibidos;
                tarja.Factor = Math.Round(tarja.Factor.Value, 3, MidpointRounding.AwayFromZero);
                tarja.DiferenciaMetros = Math.Round(tarja.DiferenciaMetros.Value, 3, MidpointRounding.AwayFromZero);
                tarja.MetrosCubicosRecibidos = Math.Round(tarja.MetrosCubicosRecibidos.Value, 3, MidpointRounding.AwayFromZero);
            }
            else
            {
                tarja.MetrosCubicosRecibidos = 0;
                tarja.Cabezas = 0;
                tarja.DiferenciaTrozos = 0;
                tarja.DiferenciaMetros = 0;
                tarja.Factor = 0;
                tarja.DiferenciaMetros = 0;
                tarja.MetrosCubicosRecibidos = 0;
            }
            tarja.Estatus = EEstatus.CERRADO;
            
            _unitOfWork.troceriaHeaderRepository.Update(tarja);
            _unitOfWork.Guardar();

        }
    }
}
