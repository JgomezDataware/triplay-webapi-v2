﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class Impresora : EntidadBase
    {
        public string DireccionIp { get; set; }
        public string DireccionPort { get; set; }
        public EColor Descripcion { get; set; }
        public bool Troceria { get; set; }
    }
}
