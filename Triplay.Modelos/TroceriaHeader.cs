﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{

    public class TroceriaHeader : EntidadBase
    {

        public DateTime Fecha { get; set; } = DateTime.Now;
        public string? Origen { get; set; }
        public string? Propietario { get; set; }
        public int OperadorId { get; set; }
        public Operadores? Operador { get; set; }
        public int BoletoId { get; set; }
        [ForeignKey("BoletoId")]
        public Boleto? Boleto { get; set; }
        [NotNull]
        public int? TrozosRecibidos { get; set; } = 0;
        [NotNull]
        public int? Cabezas { get; set; } = 0;
        [NotNull]
        public double? MetrosCubicosRecibidos { get; set; } = 0;
        [NotNull]
        public double? Factor { get; set; } = 0;
        [NotNull]
        public int? DiferenciaTrozos { get; set; } = 0;
        [NotNull]
        public double? DiferenciaMetros { get; set; } = 0;
        public string? GC { get; set; }
        public string? GAC { get; set; }
        public string? GA { get; set; }
        public string? GAD { get; set; }
        public EEstatus Estatus { get; set; } = 0;






    }
}
