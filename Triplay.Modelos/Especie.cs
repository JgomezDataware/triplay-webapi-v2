﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    public class Especie:EntidadBase
    {
        public string Nombre { get; set; }
        public string? Codigo { get; set; } = String.Empty;
    }
}
