﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    public class Gruas:EntidadBase
    {
        [Required]
        [NotNull]
        public string Codigo { get; set; }
        [NotNull]
        [Required]
        public string Nombre { get; set; } 
        public string? CardCode { get; set; }
    }
}
