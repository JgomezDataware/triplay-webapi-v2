﻿namespace Triplay.Modelos
{
    public class IpCamaraSettigs
    {
     
        public string AuthType { get; set;} = string.Empty;
        public IEnumerable<CamSettings> CamSettings { get; set; }
        public string SnapshotPath { get; set; }
        public string FilePath { get; set; } = string.Empty;
        public int ChannelsCount { get; set; }
    }

    public class CamSettings
    {
 
       public string url { get; set; }
       public string user { get; set; }
       public string password { get; set; }    
    }

}
