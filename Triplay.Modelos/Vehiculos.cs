﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Modelos
{
    public class Vehiculos:EntidadBase
    { 
        public string Placas { get;set; }
      
        public int OperadorId { get; set; }
        public int EmpresaId { get; set; }
        [ForeignKey("EmpresaId")]
        public Empresa? Empresa { get; set; }

        public DateTime Fecha { get; set; } = DateTime.Now;
    }
}
