﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos.Enums
{
    public enum EClasificacion
    {
        AV = 0,
        TV = 1,
        TC = 2,
        AB = 3,
        AM = 4,
        CD = 5,
        SB = 6,
        SM = 7,
        LN = 8
    }
}
