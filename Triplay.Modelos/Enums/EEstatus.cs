﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos.Enums
{
    public enum EEstatus
    {
        ABIERTO = 0, 
        CERRADO = 1,
        COMPLETO = 2,
        RECHADAZO = 3,
    }
}
