﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos.Enums
{
    public enum EETronco
    {
        DISPONIBLE = 0,
        CORTE = 1,
        MAQUILA =2,
        BORRADO =3,
    }
}
