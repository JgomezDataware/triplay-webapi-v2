﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos.Enums
{
    public enum EColor
    {
        Blanco = 0,
        Verde = 1,
        Rojo = 2,
    }
}
