﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos.Enums
{
    public enum ECorte
    {
        Disponible=0,
        Aserradero = 1,
        Triplay  = 2
    }
}
