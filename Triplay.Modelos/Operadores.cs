﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class Operadores:EntidadBase
    {
      
        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(10)]
        public string Acceso { get; set; }
        [Required]
        public ETurno Turno { get; set; }
        [Required]
        public EUsuario Tipo_Usuario { get; set; }

    }
}
