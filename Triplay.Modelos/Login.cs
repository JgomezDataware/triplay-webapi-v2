﻿namespace Triplay.Modelos
{
    public class Login
    {
        public string CompanyDB { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
