﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class BusinessPartnerSL
    {
        public string CardName { get; set; } = string.Empty;
        public string CardType { get; } = "S";
        public string FederalTaxID { get; set; } = "XAXX010101000";
        public string Frozen { get; } = "Y";
        public int Series { get; } = 80;
        public SapYorN Properties1 { get; set; } = SapYorN.N;
        public SapYorN Properties2 { get; set; } = SapYorN.N;

    }
}
