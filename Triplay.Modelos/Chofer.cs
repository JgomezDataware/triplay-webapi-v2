﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    public class Chofer :EntidadBase
    {
        public string Nombre { get; set; }

        public int VehiculoId { get; set; }
        [ForeignKey("VehiculoId")]
        public Vehiculos? Vehiculos { get; set; }
    }
}
