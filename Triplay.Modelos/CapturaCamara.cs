﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    public class CapturaCamara
    {
        public string NombreImagen { get; set; }
        public string UrlImagen { get; set; }
    }
}
