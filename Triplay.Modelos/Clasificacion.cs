﻿using System.Diagnostics.CodeAnalysis;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class Clasificacion: EntidadBase
    {
        [NotNull]
        public string Nombre { get; set; }
        [NotNull]
        public EClasificacion Tipo { get; set; }

    }
}
