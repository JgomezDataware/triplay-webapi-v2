﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class Cortes : EntidadBase
    {
        [Required]
        public double D1 { get; set; }
        [Required]
        public double D2 { get; set; }
        [Required]
        public double LAR { get; set; }
        [Required]
        public int EspecieId { get; set; }
        [ForeignKey("EspecieId")]
        public Especie? Especie { get; set; }
        [Required]
        public int TroncoId { get; set; }
        [ForeignKey("TroncoId")]
        public TroceriaDetalle? Tronco { get; set; }
        [NotNull]
        public EClasificacion Clasificacion { get; set; }
        [NotNull]
        public int? Descuento { get; set; } = 0;
       
        [MaxLength(30)]
        public string Etiqueta { get; set; } = string.Empty;
        public EColor Color { get; set; } = 0;
    }
}
