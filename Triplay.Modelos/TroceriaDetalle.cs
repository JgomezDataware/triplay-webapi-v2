﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class TroceriaDetalle : EntidadBase
    {
        public int TroceriaId { get; set; }
        [ForeignKey("TroceriaId")]
        public TroceriaHeader? TroceriaHeader { get; set; }
        public int EspecieId { get; set; }
        [ForeignKey("EspecieId")]
        public Especie? Especie { get; set; }
        [Required]
        
        public double D1 { get; set; }
        [Required]
        public double D2 { get; set; }
        [Required]
        public double LAR { get; set; }
        [Required]

        public double DES { get; set; } = 0;
        [NotNull]

        public double? AV { get; set; } = 0;
        [NotNull]

        public double? TV { get; set; } = 0;
        [NotNull]

        public double? TC { get; set; } = 0;
        [NotNull]

        public double? AB { get; set; } = 0;
        [NotNull]

        public double? AM { get; set; } = 0;
        [NotNull]

        public double? CD { get; set; } = 0;
        [NotNull]

        public double? SB { get; set; } = 0;
        [NotNull]

        public double? SM { get; set; } = 0;
        [NotNull]

        public double? LN { get; set; } = 0;
        [NotNull]

        public bool? CAB { get; set; } = false;
        [NotNull]

        public double? MetrosCubicos { get; set; } = 0;
        [NotNull]
        public double? pies { get; set; } = 0;
        [MaxLength(30)]
        public string Etiqueta { get; set; } = string.Empty;
        public EETronco estatus { get; set; } = 0;
        public int Consecutivo { get; set; }


    }
}
