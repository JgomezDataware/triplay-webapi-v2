﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    [Keyless]
    public class ConcentradoBasicoHeader
    {
        public string Placas { get; set; }
        public string Origen { get; set; }
        public string GuiaForestal { get; set; }
        public string Tarja { get; set; }
        public int Trozos { get; set; }
        public int TrozosRecibidos { get; set; }
        public double MetrosCubicos { get; set; }
        public double MetrosCubicosRecibidos { get; set; }
        public double DiferenciaMetros { get; set; }
        public double PiesEnviados { get; set; }
        public double PiesRecibidos { get; set; }
        public double DiferenciaPies { get; set; }
        public double PesoNeto { get; set; }
    }
}
