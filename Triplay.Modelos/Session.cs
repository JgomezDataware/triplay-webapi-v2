﻿using Newtonsoft.Json;

namespace Triplay.Modelos
{
    public class Session
    {

        [JsonProperty("odata.metadata")]
        public string odatametadata { get; set; }
        public string SessionId { get; set; }
        public string Version { get; set; }
        public int SessionTimeout { get; set; }

    }
}
