﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{
    public class ConcentradoBasicoDetalle
    {
        public double TAVM3 { get; set; }
        public double TTVM3 { get; set; }
        public double TTCM3 { get; set; }
        public double TABM3 { get; set; }
        public double TAMM3 { get; set; }
        public double TCDM3 { get; set; }
        public double TSBM3 { get; set; }
        public double TSMM3 { get; set; }
        public double TLNM3 { get; set; }
        [Key]
        public string Especie { get; set; } 

    }
}
