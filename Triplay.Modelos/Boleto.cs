﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Triplay.Modelos.Enums;

namespace Triplay.Modelos
{
    public class Boleto : EntidadBase
    {
        [Required]
        ///
        /// prop
        ///
        public int EmpresaId { get; set; }
        [ForeignKey("EmpresaId")]
        public Empresa? Empresa { get; set; }
        [Required]
        public int ProductoId { get; set; }
        [ForeignKey("ProductoId")]
        public Producto? Producto { get; set; }
        [Required]
        public int PrimerOperador { get; set; }
        //public Operadores? Operador1 { get; set; }
        public int? SegundoOperador { get; set; }
        // public Operadores? Operador2 { get; set; }
        [Required]
        public int ChoferId { get; set; }
        [ForeignKey("ChoferId")]
        public Chofer? Chofer { get; set; }
        public string Placas { get; set; }
        public double PrimeraPesada { get; set; }
        public double? SegundaPesada { get; set; }
        public DateTime FechaPrimeraPesada { get; set; }
        public DateTime? FechaSegundaPesada { get; set; }
        public double? PesoNeto { get; set; }
        public string PrimerTipo { get; set; }
        public string? SegundoTipo { get; set; }
        public string CapturasPrimeraPesada { get; set; } = String.Empty;
        public string CapturasSegundaPesada { get; set; } = String.Empty;
        public string? GuiaForestal { get; set; }
        public string? GuiaForestalRuta { get; set; }
        [MaxLength(30)]
        public string? FolioConsecutivo { get; set; } = String.Empty;
        [MaxLength(40)]
        public string? Almacen { get; set; } = String.Empty;
        public string? Tarja { get; set; }
        public string? TarjaRuta { get; set; }
        public double? metroscubicos { get; set; }
        public int? Trozos { get; set; }
        public bool EsTroceria { get; set; } = false;
        public EEstatus Estatus { get; set; } = EEstatus.ABIERTO;
    }
}
 