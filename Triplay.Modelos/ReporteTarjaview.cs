﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triplay.Modelos
{

    public class ReporteTarjaview
    {

        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Tarja { get; set; }
        public string GuiaForestal { get; set; }
        public string Placas { get; set; }
        public double PesoNeto { get; set; }
        public int Trozos { get; set; }
        public int Cabezas { get; set; }
        public int TrozosRecibidos { get; set; }
        public double MetrosCubicosRecibidos { get; set; }
        public double MetrosCubicosEnviados { get; set; }
        public int DiferenciaTrozos { get; set; }
        public double DiferenciaMetros { get; set; }
        public double Factor { get; set; }
        [Key]
        public int NumTronco { get; set; }
        public string Especie { get; set; }
        public double D1 { get; set; }
        public double D2 { get; set; }
        public double LAR { get; set; }
        public double DES { get; set; }
        public double AV { get; set; }
        public double TV { get; set; }
        public double TC { get; set; }
        public double AB { get; set; }
        public double AM { get; set; }
        public double CD { get; set; }
        public double SB { get; set; }
        public double SM { get; set; }
        public double LN { get; set; }
        public bool CAB { get; set; }
        public double AVM3 { get; set; }
        public double TVM3 { get; set; }
        public double TCM3 { get; set; }
        public double ABM3 { get; set; }
        public double AMM3 { get; set; }
        public double CDM3 { get; set; }
        public double SBM3 { get; set; }
        public double SMM3 { get; set; }
        public double LNM3 { get; set; }
        public double TotalM3 { get; set; }

    }
}
