﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triplay.Modelos;

namespace Triplay.Modelos
{
    public class Producto:EntidadBase
    {
       
        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }
    }
}
