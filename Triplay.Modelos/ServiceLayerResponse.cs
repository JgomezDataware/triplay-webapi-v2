﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;

namespace Triplay.Modelos
{   public class ServiceLayerResponse
    {

            [JsonProperty("odata.metadata")]
            public string odatametadata { get; set; }
            public IEnumerable value { get; set; } 
        
    }
}
