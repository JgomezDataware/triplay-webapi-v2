﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Triplay.Modelos
{ 
    public class ReporteGruas
    {
        [Key]
        public int IdTarja { get; set; }
        public string? GA { get; set; }
        public string? GC { get; set; }
        public string? GAC { get; set; }
        public string? GAD { get; set; }
        public double? Pies { get; set; }
        public string GuiaForestal { get; set; }
    }
}
